﻿namespace UI
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            BtnGetTargets = new Button();
            label1 = new Label();
            ListViewTargetsInView = new ListView();
            label2 = new Label();
            ListViewTargetsToKill = new ListView();
            BtnAddTargetToKill = new Button();
            BtnRemoveTargetToKill = new Button();
            TextFieldCustomName = new TextBox();
            BtnAddCustomNameToKill = new Button();
            BtnStart = new Button();
            BtnStop = new Button();
            label3 = new Label();
            label4 = new Label();
            CBKeyToAttack = new ComboBox();
            BCKeyToHeal = new ComboBox();
            label5 = new Label();
            TextFieldWhenToHeal = new TextBox();
            CheckReturnToStartingPoint = new CheckBox();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new Point(12, 12);
            button1.Name = "button1";
            button1.Size = new Size(94, 23);
            button1.TabIndex = 0;
            button1.Text = "Test Project";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // BtnGetTargets
            // 
            BtnGetTargets.Location = new Point(112, 12);
            BtnGetTargets.Name = "BtnGetTargets";
            BtnGetTargets.Size = new Size(100, 23);
            BtnGetTargets.TabIndex = 1;
            BtnGetTargets.Text = "Get Targets";
            BtnGetTargets.UseVisualStyleBackColor = true;
            BtnGetTargets.Click += BtnGetTargets_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 48);
            label1.Name = "label1";
            label1.Size = new Size(85, 15);
            label1.TabIndex = 2;
            label1.Text = "Targets in View";
            // 
            // ListViewTargetsInView
            // 
            ListViewTargetsInView.Location = new Point(12, 66);
            ListViewTargetsInView.Name = "ListViewTargetsInView";
            ListViewTargetsInView.Size = new Size(171, 372);
            ListViewTargetsInView.TabIndex = 3;
            ListViewTargetsInView.UseCompatibleStateImageBehavior = false;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(218, 48);
            label2.Name = "label2";
            label2.Size = new Size(77, 15);
            label2.TabIndex = 4;
            label2.Text = "Targets to Kill";
            // 
            // ListViewTargetsToKill
            // 
            ListViewTargetsToKill.Location = new Point(218, 66);
            ListViewTargetsToKill.Name = "ListViewTargetsToKill";
            ListViewTargetsToKill.Size = new Size(171, 372);
            ListViewTargetsToKill.TabIndex = 5;
            ListViewTargetsToKill.UseCompatibleStateImageBehavior = false;
            // 
            // BtnAddTargetToKill
            // 
            BtnAddTargetToKill.Location = new Point(189, 66);
            BtnAddTargetToKill.Name = "BtnAddTargetToKill";
            BtnAddTargetToKill.Size = new Size(23, 23);
            BtnAddTargetToKill.TabIndex = 6;
            BtnAddTargetToKill.Text = ">";
            BtnAddTargetToKill.UseVisualStyleBackColor = true;
            BtnAddTargetToKill.Click += BtnAddTargetToKill_Click;
            // 
            // BtnRemoveTargetToKill
            // 
            BtnRemoveTargetToKill.Location = new Point(189, 95);
            BtnRemoveTargetToKill.Name = "BtnRemoveTargetToKill";
            BtnRemoveTargetToKill.Size = new Size(23, 23);
            BtnRemoveTargetToKill.TabIndex = 7;
            BtnRemoveTargetToKill.Text = "-";
            BtnRemoveTargetToKill.UseVisualStyleBackColor = true;
            BtnRemoveTargetToKill.Click += BtnRemoveTargetToKill_Click;
            // 
            // TextFieldCustomName
            // 
            TextFieldCustomName.Location = new Point(218, 12);
            TextFieldCustomName.Name = "TextFieldCustomName";
            TextFieldCustomName.Size = new Size(171, 23);
            TextFieldCustomName.TabIndex = 8;
            // 
            // BtnAddCustomNameToKill
            // 
            BtnAddCustomNameToKill.Location = new Point(395, 11);
            BtnAddCustomNameToKill.Name = "BtnAddCustomNameToKill";
            BtnAddCustomNameToKill.Size = new Size(175, 23);
            BtnAddCustomNameToKill.TabIndex = 9;
            BtnAddCustomNameToKill.Text = "Add Custom Name to Kill";
            BtnAddCustomNameToKill.UseVisualStyleBackColor = true;
            BtnAddCustomNameToKill.Click += BtnAddCustomNameToKill_Click;
            // 
            // BtnStart
            // 
            BtnStart.Location = new Point(495, 415);
            BtnStart.Name = "BtnStart";
            BtnStart.Size = new Size(75, 23);
            BtnStart.TabIndex = 10;
            BtnStart.Text = "Start";
            BtnStart.UseVisualStyleBackColor = true;
            BtnStart.Click += BtnStart_Click;
            // 
            // BtnStop
            // 
            BtnStop.Location = new Point(414, 415);
            BtnStop.Name = "BtnStop";
            BtnStop.Size = new Size(75, 23);
            BtnStop.TabIndex = 11;
            BtnStop.Text = "Stop";
            BtnStop.UseVisualStyleBackColor = true;
            BtnStop.Click += BtnStop_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(395, 48);
            label3.Name = "label3";
            label3.Size = new Size(77, 15);
            label3.TabIndex = 12;
            label3.Text = "Key to Attack";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(395, 77);
            label4.Name = "label4";
            label4.Size = new Size(67, 15);
            label4.TabIndex = 13;
            label4.Text = "Key to Heal";
            label4.Visible = false;
            // 
            // CBKeyToAttack
            // 
            CBKeyToAttack.FormattingEnabled = true;
            CBKeyToAttack.Items.AddRange(new object[] { "None", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9" });
            CBKeyToAttack.Location = new Point(478, 45);
            CBKeyToAttack.Name = "CBKeyToAttack";
            CBKeyToAttack.Size = new Size(92, 23);
            CBKeyToAttack.TabIndex = 14;
            CBKeyToAttack.SelectedIndexChanged += CBKeyToAttack_SelectedIndexChanged;
            // 
            // BCKeyToHeal
            // 
            BCKeyToHeal.FormattingEnabled = true;
            BCKeyToHeal.Items.AddRange(new object[] { "None", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9" });
            BCKeyToHeal.Location = new Point(478, 74);
            BCKeyToHeal.Name = "BCKeyToHeal";
            BCKeyToHeal.Size = new Size(93, 23);
            BCKeyToHeal.TabIndex = 15;
            BCKeyToHeal.Visible = false;
            BCKeyToHeal.SelectedIndexChanged += BCKeyToHeal_SelectedIndexChanged;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(445, 103);
            label5.Name = "label5";
            label5.Size = new Size(79, 15);
            label5.TabIndex = 16;
            label5.Text = "When to Heal";
            label5.Visible = false;
            // 
            // TextFieldWhenToHeal
            // 
            TextFieldWhenToHeal.Location = new Point(395, 121);
            TextFieldWhenToHeal.Name = "TextFieldWhenToHeal";
            TextFieldWhenToHeal.Size = new Size(175, 23);
            TextFieldWhenToHeal.TabIndex = 17;
            TextFieldWhenToHeal.Visible = false;
            TextFieldWhenToHeal.TextChanged += TextFieldWhenToHeal_TextChanged;
            // 
            // CheckReturnToStartingPoint
            // 
            CheckReturnToStartingPoint.AutoSize = true;
            CheckReturnToStartingPoint.Location = new Point(395, 390);
            CheckReturnToStartingPoint.Name = "CheckReturnToStartingPoint";
            CheckReturnToStartingPoint.Size = new Size(150, 19);
            CheckReturnToStartingPoint.TabIndex = 18;
            CheckReturnToStartingPoint.Text = "Return to Starting Point";
            CheckReturnToStartingPoint.UseVisualStyleBackColor = true;
            CheckReturnToStartingPoint.CheckedChanged += CheckReturnToStartingPoint_CheckedChanged;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(583, 450);
            Controls.Add(CheckReturnToStartingPoint);
            Controls.Add(TextFieldWhenToHeal);
            Controls.Add(label5);
            Controls.Add(BCKeyToHeal);
            Controls.Add(CBKeyToAttack);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(BtnStop);
            Controls.Add(BtnStart);
            Controls.Add(BtnAddCustomNameToKill);
            Controls.Add(TextFieldCustomName);
            Controls.Add(BtnRemoveTargetToKill);
            Controls.Add(BtnAddTargetToKill);
            Controls.Add(ListViewTargetsToKill);
            Controls.Add(label2);
            Controls.Add(ListViewTargetsInView);
            Controls.Add(label1);
            Controls.Add(BtnGetTargets);
            Controls.Add(button1);
            Name = "Form1";
            Text = "Random UI";
            FormClosing += Form1_FormClosing;
            FormClosed += Form1_FormClosed;
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private Button BtnGetTargets;
        private Label label1;
        private ListView ListViewTargetsInView;
        private Label label2;
        private ListView ListViewTargetsToKill;
        private Button BtnAddTargetToKill;
        private Button BtnRemoveTargetToKill;
        private TextBox textBox1;
        private Button BtnAddCustomNameToKill;
        private Button BtnStart;
        private Button BtnStop;
        private TextBox TextFieldCustomName;
        private Label label3;
        private Label label4;
        private ComboBox CBKeyToAttack;
        private ComboBox BCKeyToHeal;
        private Label label5;
        private TextBox TextFieldWhenToHeal;
        private CheckBox CheckReturnToStartingPoint;
    }
}
using System.IO.Pipes;
using System.Runtime.InteropServices;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace UI
{
    public partial class Form1 : Form
    {
        // Allocate a console window
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        // Redirect the standard input/output streams to the console window
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AttachConsole(int dwProcessId);

        private const int ATTACH_PARENT_PROCESS = -1;

        private NamedPipeClientStream pipeClientIn;
        private NamedPipeClientStream pipeClientOut;
        private List<string> targetsInViewList = new List<string>();



        public Form1()
        {
            InitializeComponent();

            // oh god dat designer T_T

            ListViewTargetsInView.View = View.Details;

            // Remove existing columns
            while (ListViewTargetsInView.Columns.Count > 1)
            {
                ListViewTargetsInView.Columns.RemoveAt(1);
            }

            ColumnHeader columnHeader = new ColumnHeader();
            columnHeader.Text = "Target Name";
            columnHeader.Width = -2; // Set width to -1 for automatic resizing
            ListViewTargetsInView.Columns.Add(columnHeader);


            ListViewTargetsToKill.View = View.Details;

            // Remove existing columns
            while (ListViewTargetsToKill.Columns.Count > 1)
            {
                ListViewTargetsToKill.Columns.RemoveAt(1);
            }

            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "Target Name";
            columnHeader2.Width = -2; // Set width to -1 for automatic resizing
            ListViewTargetsToKill.Columns.Add(columnHeader2);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // Allocate a console window
            AllocConsole();

            // Redirect standard input/output streams to the console window
            AttachConsole(ATTACH_PARENT_PROCESS);

            bool startApplication = true;
            /*
            try
            {
                Process? process = Process.Start("stdmaptest.exe", "");
                Thread.Sleep(1000);
                if (process == null)
                {
                    MessageBox.Show("process stdmaptest.exe doesn't exist");
                    Application.Exit();
                }
                else if (process.HasExited)
                {
                    MessageBox.Show("can't start stdmaptest.exe");
                    Application.Exit();
                }
                else
                {
                    startApplication = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("failed to start stdmaptest.exe");
                Application.Exit();
            }
            */

            if (startApplication)
            {
                pipeClientOut = new NamedPipeClientStream(".", "CWstdmaptestv0.1In", PipeDirection.Out);
                Console.WriteLine("UI >> Attempting to connect to the named pipe...");
                pipeClientOut.Connect();
                Console.WriteLine("UI >> Connected!");

                Task.Run(() =>
                {
                    pipeClientIn = new NamedPipeClientStream(".", "CWstdmaptestv0.1Out", PipeDirection.In);
                    Console.WriteLine("UI >> Attempting to connect to the named pipe...");
                    pipeClientIn.Connect();
                    Console.WriteLine("UI >> Connected!");

                    for (; ; Thread.Sleep(10))
                    {
                        byte[] buffer = new byte[1024 * 1000];
                        int bytesRead = pipeClientIn.Read(buffer, 0, buffer.Length);
                        string receivedMessage = Encoding.UTF8.GetString(buffer, 0, bytesRead);


                        var dbgData = receivedMessage.Split("\n");
                        Console.WriteLine("UI >> Received data: \n" + dbgData.First() + " " + (dbgData.Length > 1 ? dbgData[1] : ""));

                        if (receivedMessage.StartsWith("[TargetsInView]"))
                        {
                            targetsInViewList.Clear();
                            foreach (string message in receivedMessage.Substring("[TargetsInView]\n".Length).Split("\n\n"))
                            {
                                if (message.Contains("m_bPlayer: true") == true)
                                {
                                    continue;
                                }

                                if (message.Contains("m_dwType: 5") == false)
                                {
                                    continue;
                                }

                                foreach (string line in message.Split("\n"))
                                {
                                    if (line.StartsWith("m_szName: "))
                                    {
                                        var targetName = line.Substring("m_szName: ".Length);
                                        if (targetsInViewList.Contains(targetName) == false)
                                        {
                                            targetsInViewList.Add(targetName);
                                        }
                                        break;
                                    }
                                }
                            }

                            UpdateListViewTargetsInView();
                        }
                        else if (receivedMessage.StartsWith("[TargetsToKill]"))
                        {
                            ListViewTargetsToKill.Items.Clear();
                            foreach (string message in receivedMessage.Substring("[TargetsToKill]\n".Length).Split("\n"))
                            {
                                ListViewTargetsToKill.Items.Add(message);
                            }
                            UpdateListViewTargetsInView();
                        }
                        else if (receivedMessage.StartsWith("[BotStatus]"))
                        {
                            if (receivedMessage.Substring("[BotStatus]\n".Length).Trim().StartsWith("true"))
                            {
                                EnableStopBotUI();
                            }
                            else
                            {
                                EnableStartBotUI();
                            }

                            SendMsg("[GetKeyToAttack]");
                        }
                        else if (receivedMessage.StartsWith("[KeyToAttack]"))
                        {
                            CBKeyToAttack.SelectedIndex = Convert.ToInt32(receivedMessage.Substring("[KeyToAttack]\n".Length));

                            SendMsg("[GetReturnToStartingPoint]");
                        }
                        else if (receivedMessage.StartsWith("[ReturnToStartingPoint]"))
                        {
                            if (receivedMessage.Substring("[ReturnToStartingPoint]\n".Length).Trim().StartsWith("true"))
                            {
                                SetCheckReturnToStartingPointUI(true);
                            }
                            else
                            {
                                SetCheckReturnToStartingPointUI(false);
                            }

                            GetTargetToKill();
                        }
                        // exit if the pipe returns emptys
                        else if (string.IsNullOrEmpty(receivedMessage) == true || pipeClientIn.IsConnected || pipeClientOut.IsConnected)
                        {
                            Application.Exit();
                        }

                    }
                });

                SendMsg("[GetBotStatus]");
            }
            else
            {
                Task.Run(() =>
                {
                    Thread.Sleep(1000);
                    Application.Exit();
                });
            }

        }

        private void UpdateListViewTargetsInView()
        {
            ListViewTargetsInView.Items.Clear();
            foreach (var target in targetsInViewList)
            {
                if (ListViewTargetsToKill.Items.Cast<ListViewItem>().Select(x => x.Text).Contains(target) == false)
                {
                    ListViewTargetsInView.Items.Add(target);
                }
            }
        }

        private void EnableStopBotUI()
        {
            BtnStop.Enabled = true;
            BtnStart.Enabled = false;
        }

        private void EnableStartBotUI()
        {
            BtnStart.Enabled = true;
            BtnStop.Enabled = false;
        }

        private void SetCheckReturnToStartingPointUI(bool state)
        {
            CheckReturnToStartingPoint.CheckedChanged -= CheckReturnToStartingPoint_CheckedChanged;
            if (state == true)
            {
                CheckReturnToStartingPoint.CheckState = CheckState.Checked;
            }
            else
            {
                CheckReturnToStartingPoint.CheckState = CheckState.Unchecked;
            }
            CheckReturnToStartingPoint.CheckedChanged += CheckReturnToStartingPoint_CheckedChanged;
        }

        private void SendMsg(string msg)
        {
            var messageBytes = Encoding.UTF8.GetBytes(msg).ToList(); messageBytes.Add(0x00);
            pipeClientOut.Write(messageBytes.ToArray(), 0, messageBytes.Count);
            pipeClientOut.Flush();
        }

        private void GetTargetToKill()
        {
            SendMsg("[GetTargetsToKill]");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendMsg("[TestProject]");
        }

        private void BtnGetTargets_Click(object sender, EventArgs e)
        {
            SendMsg("[GetTargets]");
        }

        private void BtnAddTargetToKill_Click(object sender, EventArgs e)
        {
            foreach (var item in ListViewTargetsInView.SelectedItems.Cast<ListViewItem>())
            {
                if (string.IsNullOrEmpty(item.Text))
                {
                    continue;
                }

                SendMsg("[AddTargetToKill]\n" + item.Text);
            }

            GetTargetToKill();
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            SendMsg("[StartBot]");
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            SendMsg("[StopBot]");
        }

        private void BtnRemoveTargetToKill_Click(object sender, EventArgs e)
        {
            foreach (var item in ListViewTargetsToKill.SelectedItems.Cast<ListViewItem>())
            {
                if (string.IsNullOrEmpty(item.Text))
                {
                    continue;
                }

                SendMsg("[RemoveTargetToKill]\n" + item.Text);
            }

            GetTargetToKill();
        }

        private void BtnAddCustomNameToKill_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextFieldCustomName.Text) == false)
            {
                SendMsg("[AddTargetToKill]\n" + TextFieldCustomName.Text);
                GetTargetToKill();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClosePipes();
        }

        private void ClosePipes()
        {
            try
            {
                SendMsg("[Close]");
                pipeClientIn.Close();
                pipeClientOut.Close();
            }
            catch (Exception e)
            {
                // Nothing
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClosePipes();
        }

        private void CBKeyToAttack_SelectedIndexChanged(object sender, EventArgs e)
        {
            SendMsg("[KeyToAttack]\n" + CBKeyToAttack.SelectedIndex);
        }

        private void BCKeyToHeal_SelectedIndexChanged(object sender, EventArgs e)
        {
            SendMsg("[KeyToHeal]\n" + BCKeyToHeal.SelectedIndex);
        }

        private void TextFieldWhenToHeal_TextChanged(object sender, EventArgs e)
        {
            SendMsg("[WhenToHeal]\n" + TextFieldWhenToHeal.Text);

        }

        private void CheckReturnToStartingPoint_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckReturnToStartingPoint.Checked == true)
            {
                SendMsg("[EnableReturnToStartingPoint]");
            }
            else
            {
                SendMsg("[DisableReturnToStartingPoint]");
            }
        }
    }
}
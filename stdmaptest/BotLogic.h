#pragma once

#include "all.h"

#include <vector>
#include <string>
#include <numeric>
#include <stdio.h>

#include "Globals.h"
#include "Externals/Externals.h"

#include "Common/Ctrl.h"
#include "Common/Mover.h"

struct CaseInsensitiveCompare
{
	bool operator()(char c1, char c2) const
	{
		return std::tolower(static_cast<unsigned char>(c1)) == std::tolower(static_cast<unsigned char>(c2));
	}
};

bool CompareStringsIgnoreCase(const std::string& str1, const std::string& str2)
{
	return std::equal(str1.begin(), str1.end(), str2.begin(), str2.end(), CaseInsensitiveCompare());
}

class BotLogic {

private:
	ExternalMap<DWORD, uint32_t> m_objmap;

	D3DXVECTOR3 patrolPoint;
	float patrolRange = 0;
	int gotEnemiesOutsidePatrolPointCount = 0;

public:
	BotLogic(ExternalMap<DWORD, uint32_t> objmap) : m_objmap(objmap) {

	}

	void SetObjMap(ExternalMap<DWORD, uint32_t> objmap) {
		m_objmap = objmap;
	}

	static bool CompareName(TCHAR* name) {
		std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
		for (auto targetName : Globals::TargetsToKill) {
			//printf("%s == %s\n", targetName, sName);

			//if (lstrcmp(ConvertStringToTChar(targetName), name) == 0) {
			if (CompareStringsIgnoreCase(std::string((char*)name), targetName)) {
				return true;
			}
		}

		/*
		return std::string((char*)name).find("Aibatt") != std::string::npos
			|| std::string((char*)name).find("Mushpang") != std::string::npos
			|| std::string((char*)name).find("Easter") != std::string::npos
			|| std::string((char*)name).find("Pukupuke") != std::string::npos
			|| std::string((char*)name).find("Burudeng") != std::string::npos
			|| std::string((char*)name).find("Demian") != std::string::npos
			|| std::string((char*)name).find("Nyangnyang") != std::string::npos
			|| std::string((char*)name).find("Bang") != std::string::npos
			|| std::string((char*)name).find("Mia") != std::string::npos
			|| std::string((char*)name).find("Pumpkin") != std::string::npos
			|| std::string((char*)name).find("the Hammer") != std::string::npos
			;
			*/
	}

	static bool ValidateMover(CCtrl* ctrl, bool validateNames = true) {
		if (ctrl->GetType() == OT_MOVER) {
			auto mover = CMover(ctrl);

			if (mover.m_nHitPoint > 0
				&& (mover.m_idLastHitMover == -1 || mover.m_idLastHitMover == Globals::g_pPlayer->GetId())
				&& mover.IsDelete() == FALSE
				&& mover.m_pActMover->IsFly() == FALSE && mover.IsPlayer() == FALSE)
			{
				if (validateNames == true) {
					if (CompareName(mover.m_szName)) {
						return true;
					}

					return false;
				}

				return true;

			}
		}

		return false;
	}

	static float GetDist(D3DXVECTOR3 me, D3DXVECTOR3 target) {
		return sqrt(pow((me.x - target.x), 2) +
			pow((me.z - target.z), 2));
	}

	CCtrl* GetClosestObj(CCtrl* ctrl, std::vector<CCtrl*> objmap, bool validateMovers = true, bool checkIntersection = false) {
		float closestDist = 0;
		CCtrl* closestObj = nullptr;

		for (auto it : objmap) {
			if (ctrl->GetId() != it->GetId() && (false == validateMovers || ValidateMover(it))) {
				auto dist = GetDist(ctrl->GetPos(), it->GetPos());

				if (closestObj == nullptr || dist < closestDist) {
					if (true == checkIntersection) {
						D3DXVECTOR3 vStart = Globals::g_pPlayer->GetPos();    vStart.y += 0.5f;
						D3DXVECTOR3 vEnd = it->GetPos();                      vEnd.y += 0.5f;
						if (Globals::g_pWorld->IntersectObjLine(NULL, vStart, vEnd, FALSE, FALSE) == FALSE) {
							closestObj = it;
							closestDist = dist;
						}
					}
					else {
						closestObj = it;
						closestDist = dist;
					}
				}
			}
		}

		return closestObj;
	}

	float GetAverageClosestObjDistance(std::vector<CCtrl*> objmap, bool validateMovers = true) {
		std::vector<float> closestDistArr{ 0 }; // initiazlising with 0 to prevent divide with 0 error

		for (auto ctrl : objmap) {
			if (false == validateMovers || ValidateMover(ctrl)) {
				auto closestObj = GetClosestObj(ctrl, objmap);
				if (nullptr != closestObj) {
					closestDistArr.push_back(GetDist(ctrl->GetPos(), closestObj->GetPos()));
				}
			}
		}

		return std::accumulate(closestDistArr.begin(), closestDistArr.end(), 0) / closestDistArr.size();
	}

	D3DXVECTOR3 GetPosToPatrol(std::vector<CCtrl*> objmap, bool validateMovers = true) {
		//int maxObjsInRange = 0;
		float bestScore = 0;
		D3DXVECTOR3 res;


		for (auto ctrl : objmap) {
			int objsInRange = 0;
			float score = 0;

			if (false == validateMovers || ValidateMover(ctrl)) {
				for (auto it : objmap) {
					auto dist = GetDist(ctrl->GetPos(), it->GetPos());
					if (dist < patrolRange) {
						objsInRange += 1;
					}
				}
			}

			// getting score
			auto sc1 = GetDist(Globals::StartingPoint, ctrl->GetPos());
			sc1 = std::pow(sc1, 3);
			sc1 = std::sqrt(static_cast<float>(sc1));
			score = static_cast<float>(objsInRange) / sc1;

			if (bestScore == 0 || score > bestScore) {
				res = ctrl->GetPos();
				bestScore = score;
			}
		}

		return res;
	}

	// this function will accept valid movers only
	std::vector<CCtrl*> GetWhoIsAttackingMe(std::vector<CCtrl*> objmap) {
		std::vector<CCtrl*> res;

		for (auto it : objmap) {
			auto mover = CMover(it);

			if (mover.m_idLastHitMover == Globals::g_pPlayer->GetId()) {
				res.push_back(it);
			}
		}

		return res;
	}

	std::pair<std::vector<CCtrl*>, std::vector<CCtrl*>> GetValidMoversAndMap(bool validateNames = true) {
		auto objmap = ReadObjMap();
		std::vector<CCtrl*> validMovers;

		for (auto ctrl : objmap) {
			if (ValidateMover(ctrl, validateNames)) {
				validMovers.push_back(ctrl);
			}
		}

		return std::make_pair(validMovers, objmap);
	}

	/*
	bool IsInObjMap() {
		bool res = false;
		auto objmap = ReadObjMap();

		for (auto ctrl : objmap) {
		}
		for (auto ctrl : objmap) { delete ctrl; }

		return res;
	}
	*/

	std::pair<CCtrl*, bool> GetNextTarget() {
		CCtrl* res = nullptr;
		bool forceAttack = false;

		auto [validMovers, objmap] = GetValidMoversAndMap(false);
		//printf("GetValidMoversAndMap(false) validMovers.size %d\n", validMovers.size());

		// first check if someone is attacking use
		auto attackingUs = GetWhoIsAttackingMe(validMovers);
		if (attackingUs.empty() == false) {
			printf("GetNextTarget [] need to eliminate who is attacking us first\n");
			res = attackingUs[0];
			forceAttack = true;
		}
		else {
			// check if we are too far away
			if (Globals::ReturnToStartingPoint == true && GetDist(Globals::g_pPlayer->GetPos(), Globals::StartingPoint) > 100) {
				printf("GetNextTarget [] we got too far\n");
				res = nullptr;

				// reset patrol range
				patrolRange = 0;
			}
			else {

				// clear and get new valid movers that are name checked
				for (auto ctrl : objmap) { delete ctrl; }
				auto [_validMovers, _objmap] = GetValidMoversAndMap(true);
				validMovers = _validMovers;
				objmap = _objmap;

				//printf("GetValidMoversAndMap(true) validMovers.size %d\n", validMovers.size());

				//auto closestObj = GetClosestObj(Globals::g_pPlayer, validMovers, false, true);
				//res = closestObj;


				if (patrolRange <= 0 || gotEnemiesOutsidePatrolPointCount > (validMovers.size() / 4)) {
					//patrolRange = GetAverageClosestObjDistance(validMovers, false) * 1.5f;
					patrolRange = 20.0f;
					printf("GetNextTarget [] patrolRange = %f\n", patrolRange);

					patrolPoint = GetPosToPatrol(validMovers, false);
					printf("GetNextTarget [] patrolPoint = %f %f\n", patrolPoint.x, patrolPoint.z);

					gotEnemiesOutsidePatrolPointCount = 0;
				}

				std::vector<CCtrl*> validMoversIdPatrolArea;

				float closestObjScore = 0;
				CCtrl* closestObj = nullptr;

				for (auto it : validMovers) {
					auto distToPatrolPoint = GetDist(patrolPoint, it->GetPos());

					if (distToPatrolPoint <= patrolRange) {
						validMoversIdPatrolArea.push_back(it);
					}
					else {
						auto score = (distToPatrolPoint - patrolRange) + GetDist(Globals::g_pPlayer->GetPos(), it->GetPos());
						if (closestObj == nullptr || score < closestObjScore) {
							D3DXVECTOR3 vStart = Globals::g_pPlayer->GetPos();    vStart.y += 0.5f;
							D3DXVECTOR3 vEnd = it->GetPos();                      vEnd.y += 0.5f;
							if (Globals::g_pWorld->IntersectObjLine(NULL, vStart, vEnd, FALSE, FALSE) == FALSE) {
								closestObj = it;
								closestObjScore = score;
							}
						}
					}
				}

				if (validMoversIdPatrolArea.size() == 0) {
					// reset patrolPoint
					patrolRange = 0;
				}

				printf("GetNextTarget [] movers in range %d\n", validMoversIdPatrolArea.size());

				auto closestInPatrolArea = GetClosestObj(Globals::g_pPlayer, validMoversIdPatrolArea, false, true);
				if (nullptr != closestInPatrolArea) {
					auto closestInPatrolAreaScore = GetDist(Globals::g_pPlayer->GetPos(), closestInPatrolArea->GetPos());

					if (closestObjScore < closestInPatrolAreaScore) {
						res = closestObj;
						gotEnemiesOutsidePatrolPointCount += 1;
						printf("GetNextTarget [] gotEnemiesOutsidePatrolPointCount = %d %d\n", gotEnemiesOutsidePatrolPointCount, (validMovers.size() / 4));
					}
					else {
						res = closestInPatrolArea;
					}
				}
				else if (nullptr != closestObj) {
					res = closestObj;
					gotEnemiesOutsidePatrolPointCount += 1;
					printf("GetNextTarget [] gotEnemiesOutsidePatrolPointCount = %d %d\n", gotEnemiesOutsidePatrolPointCount, (validMovers.size() / 4));
				}

			}
		}

		// clear
		for (auto ctrl : objmap) {
			if (ctrl != res) { delete ctrl; }
		}

		return std::make_pair(res, forceAttack);
	}

	std::vector<CCtrl*> ReadObjMap() {
		std::vector<CCtrl*> res;

		for (auto it = m_objmap.begin(); it != m_objmap.end(); it = m_objmap.next(it)) {
			CCtrl* ctrl = new CCtrl(m_objmap.second(it));
			res.push_back(ctrl);
		}

		return res;
	}

	// is it valid, is intersected
	std::pair<bool, bool> IsSelectedTargetStillValid(bool ignoreIntersection = false) {
		auto focusObj = Globals::g_pWorld->GetObjFocus();
		if (focusObj->addr.value) {
			auto mover = new CMover(focusObj);

			if (ignoreIntersection == false) {
				D3DXVECTOR3 vStart = Globals::g_pPlayer->GetPos();    vStart.y += 0.5f;
				D3DXVECTOR3 vEnd = mover->GetPos();                   vEnd.y += 0.5f;

				//printf("IsSelectedTargetStillValid [] IntersectObjLine start\n");

				if (Globals::g_pWorld->IntersectObjLine(NULL, vStart, vEnd, FALSE, FALSE)) {
					printf("IsSelectedTargetStillValid [] IntersectObjLine\n");

					delete mover;
					//Globals::g_pWorld->ClearFocus();

					return std::make_pair(false, true);
				}
				else {
					//printf("IsSelectedTargetStillValid [] IntersectObjLine else\n");

					if (mover->m_idLastHitMover == Globals::g_pPlayer->GetId() || mover->m_idLastHitMover == -1) {
						delete mover;
					}
					// else someone else got target first
					else {
						printf("IsSelectedTargetStillValid [] seems someone got to target first\n");

						delete mover;
						//Globals::g_pWorld->ClearFocus();

						return std::make_pair(false, false);
					}
				}
			}

			return std::make_pair(true, false);
		}

		return std::make_pair(false, false);
	}

};

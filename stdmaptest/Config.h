#pragma once

#include <vector>
#include <string>
#include "msgpack/include/msgpack.hpp"

struct CConfig {

	std::vector<std::string> targetsToKill;
	int keytoKill;
	int keyToHeal;
	uint32_t whenToHeal;
	bool returnToStartingPoint;

	MSGPACK_DEFINE(targetsToKill, keytoKill, keyToHeal, whenToHeal, returnToStartingPoint);


	static void Write() {

	}

	static void Read();
};
#include "all.h"

#include "Globals.h"

#include "Common/WndManager.h"

uint32_t Globals::base = 0;
std::vector<PVOID> Globals::ThreadHandles{};
CMover* Globals::g_pPlayer = nullptr;
CWorld* Globals::g_pWorld = nullptr;
CWndManager* Globals::g_pWndMng = nullptr;
D3DXVECTOR3 Globals::StartingPoint = D3DXVECTOR3{};
std::vector<std::string> Globals::TargetsToKill;
int Globals::AttackKey = 1;
bool Globals::ReturnToStartingPoint = true;
std::mutex Globals::Config_mutex;


int getFunlength(void* funcaddress) {
	int length = 0;
	for (length = 0; *((UINT32*)(&((unsigned char*)funcaddress)[length])) != 0xCCCCCCCC; ++length);
	return length;
}

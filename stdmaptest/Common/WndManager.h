#pragma once
#include "../all.h"

#include <Windows.h>

#include "../Externals/Externals.h"
#include "../Offsets.h"

typedef void(__thiscall* ObjectExecutor_t)(PVOID _this, PVOID pShortcut, bool param2);

class CWndManager {

public:

	PVOID handle;
	ExternalVariable<uint32_t> addr;

public:



	CWndManager(PVOID handle) : handle(handle) {
		Update();
	}

	void Update() {

		addr = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_WND_MANAGER);

	}

	struct ObjectExecutor_Args {
		uint32_t addr;
		PVOID _this;
		PVOID pShortcut;
		bool param2;
	};

	static DWORD WINAPI ObjectExecutor_External(ObjectExecutor_Args * lpParameter) {
		auto args = static_cast<ObjectExecutor_Args*>(lpParameter);
		((ObjectExecutor_t)args->addr)(args->_this, args->pShortcut, args->param2);

		return 0;

		FUN_END
	}

	void ObjectExecutor(uint32_t pShortcut) {
		ObjectExecutor_Args args{ Globals::base + OFFSET_OBJECT_EXECUTOR, (PVOID)addr.addr, (PVOID)pShortcut, 0 };
		LPVOID lpArgs = static_cast<LPVOID>(&args);

		auto funPtr = &CWndManager::ObjectExecutor_External;
		auto res = RunFunctionExternally(addr.handle, funPtr, lpArgs, sizeof(ObjectExecutor_Args));
	}

};

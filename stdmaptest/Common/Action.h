#pragma once
#include "../all.h"

#include <Windows.h>

#include "../Externals/Externals.h"
#include "../Offsets.h"

#include "D3DXVECTOR3.h"


class CAction {

public:

	ExternalVariable<uint32_t> addr;

	DWORD			m_dwState;
	DWORD			m_dwStateFlag;

public:

	DWORD			GetMoveState() { return m_dwState & OBJSTA_MOVE_ALL; }
	DWORD			GetTurnState() { return m_dwState & OBJSTA_TURN_ALL; }
	DWORD			GetLookState() { return m_dwState & OBJSTA_LOOK_ALL; }
	DWORD			GetJumpState() { return m_dwState & OBJSTA_JUMP_ALL; }
	DWORD			GetDmgState() { return m_dwState & OBJSTA_DMG_ALL; }
	DWORD			GetActionState() { return m_dwState & OBJSTA_ACTION_ALL; }

	BOOL			IsState(DWORD dwState) { return (m_dwState & dwState) ? TRUE : FALSE; }
	BOOL			IsStateFlag(DWORD dwStateFlag) { return (m_dwStateFlag & dwStateFlag) ? TRUE : FALSE; }
	BOOL			IsFly() { return (m_dwStateFlag & OBJSTAF_FLY) ? TRUE : FALSE; }
	BOOL			IsDie() { return (m_dwState & OBJSTA_DIE_ALL) ? TRUE : FALSE; }
	BOOL			IsSit() { return (m_dwStateFlag & OBJSTAF_SIT) ? TRUE : FALSE; }
	BOOL			IsRun() { return ((m_dwStateFlag & OBJSTAF_WALK) == 0) ? TRUE : FALSE; }
	BOOL			IsWalk() { return (m_dwStateFlag & OBJSTAF_WALK) ? TRUE : FALSE; }
	BOOL			IsAction() { return (m_dwState & OBJSTA_ACTION_ALL) ? TRUE : FALSE; }
	BOOL			IsMove() { return (GetMoveState() == OBJSTA_FMOVE || GetMoveState() == OBJSTA_BMOVE || GetMoveState() == OBJSTA_LEFT || GetMoveState() == OBJSTA_RIGHT) ? TRUE : FALSE; }
	BOOL			IsActJump() { return m_dwState & OBJSTA_JUMP_ALL; }
	BOOL			IsActAttack() { return m_dwState & OBJSTA_ATK_ALL; }
	BOOL			IsActDamage() { return m_dwState & OBJSTA_DMG_ALL; }
	BOOL			IsActTurn() { return m_dwState & OBJSTA_TURN_ALL; }
	BOOL			IsActLook() { return m_dwState & OBJSTA_LOOK_ALL; }


	CAction() {

	}

	CAction(ExternalVariable<uint32_t> addr) : addr(addr) {
		Update();
	}

	void Update() {
		m_dwState = (addr + OFFSET_CACTION_DW_STATE).to<uint32_t>().value;
		m_dwStateFlag = (addr + OFFSET_CACTION_DW_STATE_FLAG).to<uint32_t>().value;
	}

	

};

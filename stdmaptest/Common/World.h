#pragma once

#include "../all.h"

#include <Windows.h>
#include <stdio.h>

#include "../Externals/Externals.h"
#include "../Offsets.h"

#include "Ctrl.h"


typedef BOOL(__thiscall* IntersectObjLine_t)(PVOID _this, D3DXVECTOR3* pOut, D3DXVECTOR3* vPos, D3DXVECTOR3* vEnd, BOOL bSkipTrans, BOOL bWithTerrain, BOOL bWithObject);

typedef void(__thiscall* SetObjFocus_t)(PVOID _this, PVOID obj);

typedef BOOL(__thiscall* CheckSelect_t)(PVOID _this, PVOID obj);



class CWorld {

public:

	PVOID handle;
	ExternalVariable<uint32_t> addr;

	CCtrl* m_pObjFocus;
	//ExternalVariable<uint32_t> m_idObjFocusOld;
	ExternalVariable<uint8_t[16]> m_pObjFocusEncryption;

	//ExternalVariable<uint32_t> m_pObjFocusCheck;
	//ExternalVariable<uint32_t> m_pObjFocusCheck2;
	//ExternalVariable<uint32_t> m_pClockCheck;
	//ExternalVariable<uint32_t> m_pClockCheck2;


public:

	CCtrl * LocalPlayer;

	void            SetObjFocus(CCtrl* pObj, BOOL bSend = TRUE);
	CCtrl*          GetObjFocus() { UpdateFocus(); return m_pObjFocus; }
	void            ClearFocus() { m_pObjFocus->addr << 0; }


	CWorld(PVOID handle) : handle(handle), LocalPlayer(nullptr), m_pObjFocus(nullptr) {
		Update();
	}


	void Update();
	void UpdateFocus();

	struct IntersectObjLine_Args {
		uint32_t addr;
		PVOID _this;
		D3DXVECTOR3* pOut;
		D3DXVECTOR3* vPos;
		D3DXVECTOR3* vEnd;
		BOOL bSkipTrans;
		BOOL bWithTerrain;
		BOOL bWithObject;
	};

	static DWORD WINAPI IntersectObjLine_External(IntersectObjLine_Args* lpParameter);
	BOOL IntersectObjLine(D3DXVECTOR3* pOut, D3DXVECTOR3 vPos, D3DXVECTOR3 vEnd, BOOL bSkipTrans = FALSE, BOOL bWithTerrain = FALSE, BOOL bWithObject = TRUE);

	/*
	struct SetObjFocus_Args {
		uint32_t addr;
		PVOID _this;
		PVOID idTarget;
	};
	
	static DWORD WINAPI SetObjFocus_External(SetObjFocus_Args* lpParameter);
	void SetObjFocusE(PVOID obj);


	struct CheckSelect_Args {
		uint32_t addr;
		PVOID _this;
		PVOID obj;
	};

	static DWORD WINAPI CheckSelect_External(CheckSelect_Args* lpParameter);
	BOOL CheckSelect(PVOID obj);
	*/
};

#include "World.h"
#include "../Globals.h"

#include <Windows.h>
#include <stdint.h>
#include <array>

#include "Rijndael.h"
#include "../Externals/Externals.h"


void CWorld::Update() {
	auto localPlayerAddr = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_ME);

	if (LocalPlayer != nullptr) { delete LocalPlayer; }
	LocalPlayer = new CCtrl(localPlayerAddr);

	addr = ExternalVariable<uint32_t>(handle, localPlayerAddr.value + OFFSET_COBJ_CWORLD);

	//m_pObjFocusCheck = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_FOCUS_CHECK);
	//m_pObjFocusCheck2 = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_FOCUS_CHECK2);
	//m_pClockCheck = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_CLOCK_CHECK);
	//m_pClockCheck2 = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_CLOCK_CHECK2);

	if (m_pObjFocus != nullptr) { delete m_pObjFocus; m_pObjFocus = nullptr; }
	UpdateFocus();
}

void CWorld::UpdateFocus() {
	if (nullptr == m_pObjFocus) {
		m_pObjFocus = new CCtrl(addr + OFFSET_CWORLD_FOCUS);
		//m_idObjFocusOld = addr + OFFSET_CWORLD_ID_FOCUS_OLD;
		m_pObjFocusEncryption = ExternalVariable<uint8_t[32]>(handle, addr.value + OFFSET_CWORLD_FOCUS_ENCRYPTION);

		// just debug
		// printf("::world focus enc %08X\n", addr.value + OFFSET_CWORLD_FOCUS_ENCRYPTION);
		// printf("::world focus enc %08X\n", m_pObjFocusEncryption.addr);
	}
	else {
		m_pObjFocus->Update();
		//m_idObjFocusOld.update();
	}
}

void CWorld::SetObjFocus(CCtrl* pObj, BOOL bSend)
{
	UpdateFocus();

	//if (m_pObjFocus->addr.value != 0) {
	//	m_idObjFocusOld << m_pObjFocus->GetId();
	//}
	//m_pObjFocusCheck << pObj;
	//m_pObjFocusCheck2 << pObj->GetId();


    //auto tttemp = ExternalVariable<uint32_t>(addr.handle, m_pObjFocus->addr.addr + 4);
    //tttemp << pObj->GetId();
    //printf("tttemp : %08X\n", tttemp.addr);

	// also add enctyped thingy
    char buffer[32]{};
    std::fill(buffer, buffer + 32, 0);
    int ret = snprintf(buffer, sizeof(buffer), "%ld", pObj->m_objId);

    uint8_t _StringEnc[32]{};
    std::fill(_StringEnc, _StringEnc + 32, 0);

    CRijndael::GetInstance()->ResetChain();
    CRijndael::GetInstance()->Encrypt(buffer, (char*)_StringEnc, 16, CRijndael::CBC);

    // debug output
    // for (int i = 0; i < 16; i++) {
    //     printf("%02X", _StringEnc[i]);
    // }
    // printf("\n");
    // printf("select: %ld\n", m_pObjFocus->m_objId);

    this->m_pObjFocusEncryption << _StringEnc;

	*m_pObjFocus << pObj;

	//m_pObjFocusCheck << pObj->addr.value;
	DWORD check2Value = pObj->addr.value & 0xFFFFFF00;
	//m_pObjFocusCheck2 << check2Value;
}


DWORD WINAPI CWorld::IntersectObjLine_External(IntersectObjLine_Args* lpParameter) {
	auto args = static_cast<IntersectObjLine_Args*>(lpParameter);
	auto res = 0;

	if (args->_this != nullptr && args->vPos != nullptr && args->vEnd != nullptr) {
		res = ((IntersectObjLine_t)args->addr)(args->_this, args->pOut, args->vPos, args->vEnd, args->bSkipTrans, args->bWithTerrain, args->bWithObject);
	}

	return res == TRUE ? 1 : 0;

	FUN_END
}

BOOL CWorld::IntersectObjLine(D3DXVECTOR3* pOut, D3DXVECTOR3 vPos, D3DXVECTOR3 vEnd, BOOL bSkipTrans, BOOL bWithTerrain, BOOL bWithObject) {
	auto vPos_e = ExternalVariable<D3DXVECTOR3>::Alloc(addr.handle) << vPos;
	auto vEnd_e = ExternalVariable<D3DXVECTOR3>::Alloc(addr.handle) << vEnd;

	//printf("size: %d\n", sizeof(vPos_e.value));
	//printf("addr: %08X\n", vPos_e.addr);

	IntersectObjLine_Args args{ Globals::base + OFFSET_INTERSECT_OBJ_LINE, (PVOID)addr.value, pOut, (D3DXVECTOR3*)vPos_e.addr, (D3DXVECTOR3*)vEnd_e.addr, bSkipTrans, bWithTerrain, bWithObject };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	auto funPtr = &CWorld::IntersectObjLine_External;
	auto res = RunFunctionExternally(addr.handle, funPtr, lpArgs, sizeof(IntersectObjLine_Args), false, true);

	//printf("IntersectObjLine [] vPos_e.Free\n");
	vPos_e.Free();
	//printf("IntersectObjLine [] vEnd_e.Free\n");
	vEnd_e.Free();

	return res == 1;
}

/*
DWORD __stdcall CWorld::SetObjFocus_External(SetObjFocus_Args* lpParameter) {
	auto args = static_cast<SetObjFocus_Args*>(lpParameter);

	((SetObjFocus_t)args->addr)(args->_this, args->idTarget);

	return 0;
}

void CWorld::SetObjFocusE(PVOID obj) {
	SetObjFocus_Args args = { Globals::base + OFFSET_SET_OBJ_FOCUS, (PVOID)addr.addr, obj };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CWorld::SetObjFocus_External, lpArgs, sizeof(SetObjFocus_Args));
}

DWORD __stdcall CWorld::CheckSelect_External(CheckSelect_Args* lpParameter) {
	auto args = static_cast<CheckSelect_Args*>(lpParameter);

	if (((CheckSelect_t)args->addr)(args->_this, args->obj)) {
		return 1;
	}

	return 0;
}

BOOL CWorld::CheckSelect(PVOID obj) {
	CheckSelect_Args args = { Globals::base + OFFSET_CHECK_SELECT, (PVOID)addr.addr, obj };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	return RunFunctionExternally(addr.handle, &CWorld::SetObjFocus_External, lpArgs, sizeof(CheckSelect_Args)) == 1;
}
*/

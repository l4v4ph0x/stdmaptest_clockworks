#include "DPClient.h"
#include "../Globals.h"

#include "../Externals/Externals.h"
#include "../Offsets.h"


CDPClient::CDPClient(PVOID handle) : addr(ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_DPCLIENT)) {
	Update();
	ar_e = ExternalVariable<uint8_t[16408]>::Alloc(addr.handle);
}

CDPClient::CDPClient(ExternalVariable<uint32_t> addr) : addr(addr) {
	Update();
	ar_e = ExternalVariable<uint8_t[16408]>::Alloc(addr.handle);
}

//#pragma optimize( "[optimization-list]", {on | off} )
void CDPClient::SendMeleeAttack_Hook(OBJMSG dwAtkMsg, OBJID objid, int nParam2, int nParam3, FLOAT fVal) {
	uint32_t SendMeleeAttack_orig = ((uint32_t)this - OFFSET_DPCLIENT) + OFFSET_CDPCLIENT_SEND_MELEE_ATTACK2;

	// fVal = attack speed
	// nParam3 seems to be n-th attack
	return ((SendMeleeAttack_t)SendMeleeAttack_orig)(this, dwAtkMsg, objid, nParam2, 4, fVal);

	FUN_END
}



DWORD WINAPI CDPClient::SendSetTarget_External(SendSetTarget_Args* lpParameter) {
	auto args = static_cast<SendSetTarget_Args*>(lpParameter);
	auto base = args->base;
	int nBufSize;

	((CAr_CAr)(base + OFFSET_CAR_CAR))(args->_ar, 0x8000);

	int dpid_unknown = DPID_UNKNOWN;
	((CAr_operator_int)(base + OFFSET_CAR_OP_INT))(args->_ar, &dpid_unknown);
	DWORD code = 0x55;
	((CAr_operator_DWORD)(base + OFFSET_CAR_OP_DWORD))(args->_ar, &code);
	((CAr_operator_DWORD)(base + OFFSET_CAR_OP_DWORD))(args->_ar, &args->idTarget);
	int unknown_1 = 2;
	((CAr_operator_int)(base + OFFSET_CAR_OP_INT))(args->_ar, &unknown_1);
	DWORD unknown_2 = 0xFFFFFFFF;
	((CAr_operator_DWORD)(base + OFFSET_CAR_OP_DWORD))(args->_ar, &unknown_2);
	((CAr_operator_BYTE)(base + OFFSET_CAR_OP_BYTE))(args->_ar, &args->bClear);

	LPBYTE lpBuf = ((CAr_GetBuffer)(base + OFFSET_CAR_GET_BUFFER))(args->_ar, &nBufSize);
	((CDPMng_Send)(base + OFFSET_CDPMNG_SEND))(args->_this, (LPVOID)lpBuf, nBufSize, DPID_SERVERPLAYER);

	return 0;

	FUN_END
}

void CDPClient::SendSetTarget(OBJID idTarget, BYTE bClear) {
	SendSetTarget_Args args = { (PVOID)addr.addr, (PVOID)ar_e.addr, Globals::base, idTarget, bClear };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CDPClient::SendSetTarget_External, lpArgs, sizeof(SendSetTarget_Args));
}

DWORD __stdcall CDPClient::SendMoverFocus__External(SendMoverFocus_Args* lpParameter) {
	auto args = static_cast<SendMoverFocus_Args*>(lpParameter);

	((SendMoverFocus_t)args->addr)(args->_this, args->idTarget);

	return 0;
}

void CDPClient::SendMoverFocus(OBJID idTarget) {
	SendMoverFocus_Args args = { (PVOID)addr.addr, Globals::base + OFFSET_SEND_MOVER_FOCUS, idTarget };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CDPClient::SendMoverFocus__External, lpArgs, sizeof(SendMoverFocus_Args));
}

DWORD __stdcall CDPClient::SendMouseLeftClickOnCWndWorld_External(SendMouseLeftClickOnCWndWorld_Args* lpParameter) {
	auto args = static_cast<SendMouseLeftClickOnCWndWorld_Args*>(lpParameter);

	((SendMouseLeftClickOnCWndWorld_t)args->addr)(args->_this);

	return 0;
}

void CDPClient::SendMouseLeftClickOnCWndWorld() {
	uint32_t dsa = (0x009bb200 - 0x000d0000);
	printf("%08X\n", dsa);
	uint32_t addro = Globals::base + dsa;
	printf("%08X\n", addro);
	SendMouseLeftClickOnCWndWorld_Args args = { (PVOID)addr.addr, addro };
	args.addr = addro;
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CDPClient::SendMouseLeftClickOnCWndWorld_External, lpArgs, sizeof(SendMouseLeftClickOnCWndWorld_Args));
}


DWORD __stdcall CDPClient::SendChatMsg_External(SendChatMsg_Args* lpParameter) {
	auto args = static_cast<SendChatMsg_Args*>(lpParameter);

	((SendChatMsg_t)(args->addr))(args->_this, args->param1, args->param2);

	return 0;

	FUN_END
}

void CDPClient::SendChatMsg() {
	//auto vaaro = ExternalVariable<uint32_t>(addr.handle, Globals::base + )
	//(PVOID)0x243BA230
	SendChatMsg_Args args = { (PVOID)addr.addr, Globals::base + OFFSET_SEND_CHAT_MSG, (PVOID)0x1A015FF0, (PVOID)0x00040C2C };
	//memcpy(args.data, data, 0x2000);
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CDPClient::SendChatMsg_External, lpArgs, sizeof(SendSetTarget_Args));
}


#pragma once

#include "../all.h"

#include "../Externals/Externals.h"
#include "MsgHdr.h"
#include "dplay.h"
#include "../Offsets.h"
#include "Mover.h"


typedef void(__thiscall* SendMeleeAttack_t)(PVOID _this, OBJMSG dwAtkMsg, OBJID objid, int nParam2, int nParam3, FLOAT fVal);

typedef void(__thiscall* CAr_operator_int)(PVOID _this, int *param_1);
typedef void(__thiscall* CAr_operator_DWORD)(PVOID _this, DWORD *param_1);
typedef void(__thiscall* CAr_operator_BYTE)(PVOID _this, BYTE *param_1);
typedef LPBYTE(__thiscall* CAr_GetBuffer)(PVOID _this, int* pnBufSize);
typedef BOOL(__thiscall* CDPMng_Send)(PVOID _this, LPVOID lpData, DWORD dwDataSize, DPID dpidTo);
typedef void(__thiscall* CAr_CAr)(PVOID _this, u_int nBufSize);

typedef void(__thiscall* SendMoverFocus_t)(PVOID _this, u_long uidPlayer);

typedef void(__thiscall* SendMouseLeftClickOnCWndWorld_t)(PVOID _this);

typedef void(__thiscall* SendChatMsg_t)(PVOID _this, PVOID param1, PVOID param2);


// ok
#define OFFSET_CAR_OP_INT (0x002678a0 - 0x000d0000)
// ok
#define OFFSET_CAR_OP_DWORD (0x0020e3c0 - 0x000d0000)
// ok
#define OFFSET_CAR_OP_BYTE (0x0020f820 - 0x000d0000)
// ok
#define OFFSET_CDPMNG_SEND (0x00a0fc00 - 0x000d0000)
// ok
#define OFFSET_CAR_GET_BUFFER (0x00e4c230 - 0x000d0000)
// ok
#define OFFSET_DPCLIENT (0x01142710 - 0x000d0000)
// ok
#define OFFSET_CAR_CAR (0x00e4c100 - 0x000d0000)
// ok
#define OFFSET_SEND_MOVER_FOCUS (0x00a0fcb0 - 0x000d0000)


/*
#define	BEFORESENDSOLE( ar, dw, dpid_ )	\
	CAr ar;		\
	int nBufSize;	\
	ar << dpid_ << dw;

#define	SEND( ar, pDPMng, idTo ) \
	LPBYTE lpBuf	= ar.GetBuffer( &nBufSize );	\
	(pDPMng)->Send( (LPVOID)lpBuf, nBufSize, idTo );
*/

class CDPClient {

public:

	ExternalVariable<uint32_t> addr;

	// plus 1000 for extra
	ExternalVariable<uint8_t[16408 + 1000]> ar_e;

public:

	CDPClient() {

	}

	CDPClient(PVOID handle);

	CDPClient(ExternalVariable<uint32_t> addr);

	void Update() {

	}

	__declspec(dllexport) void __thiscall SendMeleeAttack_Hook(OBJMSG dwAtkMsg, OBJID objid, int nParam2, int nParam3, FLOAT fVal);


	struct SendSetTarget_Args {
		PVOID _this;
		PVOID _ar;
		uint32_t base;
		OBJID idTarget;
		BYTE bClear;
	};

	static DWORD WINAPI SendSetTarget_External(SendSetTarget_Args* lpParameter);
	void SendSetTarget(OBJID idTarget, BYTE bClear);


	struct SendMoverFocus_Args {
		PVOID _this;
		uint32_t addr;
		OBJID idTarget;
	};

	static DWORD WINAPI SendMoverFocus__External(SendMoverFocus_Args* lpParameter);
	void SendMoverFocus(OBJID idTarget);


	struct SendMouseLeftClickOnCWndWorld_Args {
		PVOID _this;
		uint32_t addr;
	};

	static DWORD WINAPI SendMouseLeftClickOnCWndWorld_External(SendMouseLeftClickOnCWndWorld_Args* lpParameter);
	void SendMouseLeftClickOnCWndWorld();


	struct SendChatMsg_Args {
		PVOID _this;
		uint32_t addr;
		//uint32_t id;
		PVOID param1;
		PVOID param2;
	};

	static DWORD WINAPI SendChatMsg_External(SendChatMsg_Args* lpParameter);
	void SendChatMsg();

};

#pragma once
#include "../all.h"

#include <Windows.h>

#include "../Externals/Externals.h"
#include "../Offsets.h"

#include "D3DXVECTOR3.h"

#define OT_OBJ          0
#define OT_ANI          1
#define OT_CTRL         2
#define OT_SFX          3
#define OT_ITEM         4
#define OT_MOVER        5
#define OT_REGION       6
#define OT_SHIP			7
#define OT_PATH			8
#define MAX_OBJTYPE     9


class CCtrl {

public:

	ExternalVariable<uint32_t> addr;

public:
	DWORD            m_dwFlags;

	OBJID            m_objId;
	D3DXVECTOR3      m_vPos;
	uint32_t         m_dwType;


	D3DXVECTOR3     GetPos() { return m_vPos; }
	DWORD           GetType() { return m_dwType; }
	BOOL            IsDynamicObj() { return m_dwType != OT_OBJ && m_dwType != OT_SFX; }
	//void            SetId(OBJID objid) { m_objid = objid; }
	OBJID           GetId() { return m_objId; }


	CCtrl() {
	}

	CCtrl(ExternalVariable<uint32_t> addr) : addr(addr) {
		Update();
	}

	CCtrl(CCtrl *ctrl) {
		Init(ctrl);
	}

	void Init(CCtrl *ctrl) {
		this->addr = ctrl->addr;
		// Update();
	}

	void Update() {
		m_dwFlags = (addr + OFFSET_OBJ_DW_FLAG).to<OBJID>().value;

		m_vPos = D3DXVECTOR3(
			(addr + OFFSET_COBJ_X).to<float>().value,
			(addr + (OFFSET_COBJ_X + 4)).to<float>().value,
			(addr + (OFFSET_COBJ_X + 8)).to<float>().value
		);

		m_objId = (addr + OFFSET_CCTRL_ID).to<OBJID>().value;
		m_dwType = (addr + OFFSET_COBJ_DW_TYPE).to<uint32_t>().value;

	}

	void operator<<(CCtrl *val) {
		// write to proccess memory 
		addr << val->addr;
		// and update from external process
		Update();
	}

	template<typename T>
	void operator<<(T val) {
		// write to proccess memory 
		addr << val;
		// and update from external process
		Update();
	}

};

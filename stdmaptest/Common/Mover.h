#pragma once

#include "../all.h"

#include <Windows.h>
#include <iostream>

#include "../Externals/Externals.h"
#include "../Offsets.h"

#include "D3DXVECTOR3.h"
#include "Rijndael.h"

#include "Ctrl.h"
#include "ActionMover.h"


#define	OBJTYPE_PLAYER			0x00000001		
#define	OBJTYPE_MONSTER			0x00000002		
#define OBJTYPE_CTRL			0x00000004		
#define	OBJTYPE_FLYING			0x00010000


typedef void(__thiscall* CMD_SetMeleeAttack_t)(PVOID _this, OBJID idTarget, FLOAT fRange);
typedef void(__thiscall* SetDestPos_t)(PVOID _this, D3DXVECTOR3* vDestPos, bool bForward, BOOL fTransfer);


class CMover : public CCtrl {

public:

	bool            m_bPlayer;
	uint32_t        m_idPlayer;
	CActionMover *  m_pActMover;
	uint32_t        m_nHitPoint;
	uint32_t        m_nLevel;
	uint32_t        m_nManaPoint;
	uint32_t        m_nFatiguePoint;
	uint32_t        m_dwTypeFlag;

	OBJID           m_idDest;
	//float           m_fArrivalRange;

	OBJID           m_idLastHitMover;

	TCHAR           m_szName[MAX_NAME] = L"";

	TCHAR           m_szBuffId[MAX_NAME] = L"";


	BOOL            IsNPC() { return !m_bPlayer; }
	BOOL            IsPlayer() { return m_bPlayer; }
	BOOL            IsFlyingNPC() { return (m_dwTypeFlag & OBJTYPE_FLYING) ? TRUE : FALSE; }
	BOOL            IsDelete() { return (m_dwFlags & OBJ_FLAG_DELETE) || OBJ_FLAG_DELETE == 0; }

	CMover() {
		

	}

	~CMover() {
		if (m_pActMover != nullptr) { delete m_pActMover; }
	}

	CMover(ExternalVariable<uint32_t> addr) : CCtrl(addr) {
		Init();
	}

	CMover(CCtrl *ctrl) : CCtrl(ctrl) {
		Init();
	}

	void Init() {
		m_pActMover = new CActionMover(addr + OFFSET_MOVER_ACT_MOVER);
		Update();
	}

	void Update() {
		CCtrl::Update();

		m_nHitPoint = (addr + OFFSET_HIT_POINTS).to<uint32_t>().value;
		m_nManaPoint = (addr + OFFSET_MANA_POINTS).to<uint32_t>().value;
		m_nFatiguePoint = (addr + OFFSET_FATIGUE_POINTS).to<uint32_t>().value;
		m_nLevel = (addr + OFFSET_MOVER_LEVEL).to<uint32_t>().value;
		m_bPlayer = (addr + OFFSET_IS_PLAYER_CHECK_1).to<uint8_t>().value != 0;
		m_dwTypeFlag = (addr + OFFSET_DW_FLAG_TYPE).to<uint32_t>().value;
		m_idDest = (addr + OFFSET_MOVER_ID_DEST).to<uint32_t>().value;
		m_idPlayer = (addr + OFFSET_MOVER_ID_PLAYER).to<uint32_t>().value;

		m_idLastHitMover = (addr + OFFSET_MOVER_ID_LAST_HIT_MOVER).to<OBJID>().value;

		memcpy(m_szName, (addr + OFFSET_MOVER_NAME).to<TCHAR[MAX_NAME]>().value, MAX_NAME * sizeof(TCHAR));
		memcpy(m_szBuffId, (addr + OFFSET_PLAYER_BUFF_ID).to<TCHAR[MAX_NAME]>().value, MAX_NAME * sizeof(TCHAR));
	}


	struct CMD_SetMeleeAttack_Args {
		uint32_t addr;
		PVOID _this;
		OBJID idTarget;
		FLOAT fRange;
	};

	static DWORD WINAPI CMD_SetMeleeAttack_External(CMD_SetMeleeAttack_Args* lpParameter);
	void CMD_SetMeleeAttack(OBJID idTarget, FLOAT fRange = 0.0f);


	struct SetDestPos_Args {
		uint32_t addr;
		PVOID _this;
		D3DXVECTOR3 * vDestPos;
		bool bForward = true;
		BOOL fTransfer = TRUE;
	};

	static DWORD WINAPI SetDestPos_External(SetDestPos_Args* lpParameter);
	void SetDestPos(D3DXVECTOR3 vDestPos, bool bForward = true, BOOL fTransfer = TRUE);

	
};

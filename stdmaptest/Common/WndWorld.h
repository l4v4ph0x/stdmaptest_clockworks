#pragma once
#include "../all.h"
#include "../Globals.h"

#include <Windows.h>

#include "../Externals/Externals.h"
#include "../Offsets.h"

typedef void(__thiscall* OnLButtonDown_t)(PVOID _this, int param3, uint32_t y, uint32_t x);

class CWndWorld {

public:

	PVOID handle;
	ExternalVariable<uint32_t> addr;

public:



	CWndWorld(PVOID handle) : handle(handle) {
		Update();
	}

	void Update() {

		// need to be right one, from ce 1 out of 3 has to be 00000000 at login
		addr = ExternalVariable<uint32_t>(handle, Globals::base + OFFSET_WND_WORLD);

	}

	struct OnLButtonDown_Args {
		uint32_t addr;
		PVOID _this;
		uint32_t x;
		uint32_t y;
		int param3;
	};

	static DWORD WINAPI OnLButtonDown_External(OnLButtonDown_Args* lpParameter) {
		auto args = static_cast<OnLButtonDown_Args*>(lpParameter);
		//((OnLButtonDown_t)args->addr)(args->_this, args->param3, args->y, args->x);

		auto wndWorld = (uint32_t)args->_this;
		auto arg_x = args->x;
		auto arg_y = args->y;
		auto param3 = args->param3;
		auto addr = args->addr;

		__asm {
			call lab_inner
			mov esp, ebx
			// 0x18 bc of auto args, and 3*4 for compiler pushes edi, esi, edx, and 4 because we use call lab_inner
			add esp, 0x28
			jmp inner_end
		lab_inner:
			// get return address
			mov ecx, [esp]

			mov edx, dword ptr[wndWorld]
			mov esi, addr

			sub esp, 0x630

			mov ebx, ebp
			sub ebx, 0x100

			//push ebp
			// repair devirtualized header
			//lea eax, [esp + 0x630]
			//mov[ebx + 0x1c], eax

			mov eax, arg_x
			mov [ebx + 0x18], eax

			mov eax, arg_y
			mov[ebx + 0x14], eax

			mov eax, arg_x
			mov[ebx + 0x10], eax

			mov eax, arg_y
			mov[ebx + 0xc], eax

			mov eax, param3
			mov[ebx + 0x8], eax

			mov[ebx + 0x4], ecx

			lea eax, [esp + 0x630]
			mov[ebx + 0x0], eax

			sub ebp, 0x100
			sub ebp, 0xC

			mov edi, 0x201

			//mov ebp, esp
			mov dword ptr[ebp - 0x14], edx

			call esi
			//pop ebp
		inner_end:
			xor eax, eax
			pop ebp
			ret
		};

		return 0;

		FUN_END
	}

	void OnLButtonDown(uint32_t x, uint32_t y) {
		OnLButtonDown_Args args{ Globals::base + OFFSET_ON_L_BUTTON_DOWN, (PVOID)addr.value, x, y, 1 };
		LPVOID lpArgs = static_cast<LPVOID>(&args);

		auto funPtr = &CWndWorld::OnLButtonDown_External;
		auto res = RunFunctionExternally(addr.handle, funPtr, lpArgs, sizeof(OnLButtonDown_Args));
	}

};

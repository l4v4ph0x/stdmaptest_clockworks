#pragma once


struct D3DXVECTOR3
{
	float x, y, z;

	D3DXVECTOR3() {
		this->x = 0;
		this->y = 0;
		this->z = 0;
	}

	D3DXVECTOR3(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
};

#include "Mover.h"
#include "../Globals.h"


DWORD WINAPI CMover::CMD_SetMeleeAttack_External(CMD_SetMeleeAttack_Args* lpParameter) {
	CMD_SetMeleeAttack_Args* args = static_cast<CMD_SetMeleeAttack_Args*>(lpParameter);
	((CMD_SetMeleeAttack_t)args->addr)(args->_this, args->idTarget, args->fRange);
	return 0;

	FUN_END
}

void CMover::CMD_SetMeleeAttack(OBJID idTarget, FLOAT fRange) {
	CMD_SetMeleeAttack_Args args = { Globals::base + OFFSET_CMD_SET_MELEE_ATTACK, (PVOID)addr.value, idTarget, fRange };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CMover::CMD_SetMeleeAttack_External, lpArgs, sizeof(CMD_SetMeleeAttack_Args));

}


DWORD WINAPI CMover::SetDestPos_External(SetDestPos_Args* lpParameter) {
	auto* args = static_cast<SetDestPos_Args*>(lpParameter);
	((SetDestPos_t)args->addr)(args->_this, args->vDestPos, args->bForward, args->fTransfer);

	return 0;

	FUN_END
}


void CMover::SetDestPos(D3DXVECTOR3 vDestPos, bool bForward, BOOL fTransfer) {
	auto vDestPos_e = ExternalVariable<D3DXVECTOR3>::Alloc(addr.handle) << vDestPos;

	SetDestPos_Args args = { Globals::base + OFFSET_SET_DEST_POS, (PVOID)addr.value, (D3DXVECTOR3*)vDestPos_e.addr, bForward, fTransfer };
	LPVOID lpArgs = static_cast<LPVOID>(&args);

	RunFunctionExternally(addr.handle, &CMover::SetDestPos_External, lpArgs, sizeof(SetDestPos_Args));

	vDestPos_e.Free();
}

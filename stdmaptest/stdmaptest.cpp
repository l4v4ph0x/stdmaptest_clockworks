#include "all.h"

#include <Windows.h>
#include <stdio.h>
#include <map>
#include <stdint.h>
#include <array>
#include <type_traits>
#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include <atltime.h>

#include "Globals.h"
#include "stdmaptest.h"
#include "Search.h"
#include "Externals/Externals.h"
#include "BotLogic.h"
#include "ReadWriteConfig.h"

#include "Common/Ctrl.h"
#include "Common/Mover.h"
#include "Common/World.h"
#include "Common/DPClient.h"
#include "Common/WndManager.h"
#include "Common/WndWorld.h"

float GetDist(D3DXVECTOR3 me, D3DXVECTOR3 target) {
    return sqrt(pow((me.x - target.x), 2) +
        pow((me.z - target.z), 2));
}

std::vector<std::string> writePipeQueue;
std::mutex writePipeQueue_mutex;

std::vector<std::string> pipeQueue;
std::mutex pipeQueue_mutex;

std::atomic<bool> FunnyBotRunning(false);
std::atomic<bool> CancelFunnyBot(false);
std::thread FunnyBotThread;

std::atomic<bool> InitAndTextRunning(false);
std::thread InitAndTextThread;

std::atomic<bool> GetTargetsRunning(false);
std::thread GetTargetsThread;


bool InitProject() {
    if (Globals::g_pPlayer == nullptr || Globals::g_pWorld == nullptr) {
        printf("InitProject [] clean \n");

        if (Globals::g_pPlayer != nullptr) {
            delete Globals::g_pPlayer;
        }

        if (Globals::g_pWorld != nullptr) {
            delete Globals::g_pWorld;
        }

        uint32_t pid = Search::GetProc(L"Neuz.exe");
        PVOID h = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_CREATE_THREAD, FALSE, pid);

        auto base = Search::GetModule(h, L"Neuz.exe");
        Globals::base = base;
        printf("base %08X\n", base);

        auto mainThreadId = Search::GetMainThreadId(pid);
        printf("InitProject [] mainThreadId %d hex %08X\n", mainThreadId, mainThreadId);

        // Get the main thread ID of the process
        DWORD threadId = 0;
        THREADENTRY32 te32;
        te32.dwSize = sizeof(THREADENTRY32);

        HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
        if (hSnapshot != INVALID_HANDLE_VALUE) {
            if (Thread32First(hSnapshot, &te32)) {
                do {
                    PVOID tHandle = OpenThread(THREAD_SUSPEND_RESUME, FALSE, mainThreadId);
                    if (tHandle == nullptr) {
                        std::cerr << "InitProject [] MainThreadHandle failed\n";
                    }
                    else {
                        Globals::ThreadHandles.push_back(tHandle);
                    }
                } while (Thread32Next(hSnapshot, &te32));
            }
            CloseHandle(hSnapshot);
        }

        if (h) {
            auto g_pPlayer_addr = ExternalVariable<uint32_t>(h, Globals::base + OFFSET_ME);
            CMover* g_pPlayer = new CMover(g_pPlayer_addr);
            Globals::g_pPlayer = g_pPlayer;

            CWorld* world = new CWorld(h);
            Globals::g_pWorld = world;

            printf("g_pPlayer %08X\n", g_pPlayer->addr.value);
            printf("g_pWorld %08X\n", world->addr.value);
            printf("focus addr %08X\n", world->m_pObjFocus->addr.addr);
            printf("focus encryption addr %08X\n", world->m_pObjFocusEncryption.addr);

            return true;
        }
        else {
            printf("not enough permissions\n");
        }
    }
    else {
        return true;
    }

    return false;
}

void SendAttackKey(bool debug = true) {
    if (debug) {
        printf("SendAttackKey [] Config_mutex lock\n");
    }

    int keyToAttack = 0;
    Globals::Config_mutex.lock();
    keyToAttack = Globals::AttackKey;
    Globals::Config_mutex.unlock();

    if (debug) {
        printf("SendAttackKey [] ObjectExecutor\n");
    }

    if (keyToAttack > 0 && keyToAttack <= 9) {
        auto m_pWndTaskBar = ExternalVariable<uint32_t>(Globals::g_pWorld->addr.handle, Globals::base + OFFSET_WND_TASKBAR);
        auto m_paSlotItem = m_pWndTaskBar + (OFFSET_WND_TASKBAR_SLOTITEM + ((Globals::AttackKey - 1) * SLOTITEM_SIZE));

        Globals::g_pWndMng->ObjectExecutor(m_paSlotItem.addr);
    }
}

void FunnyBot() {
    CancelFunnyBot = false;
    FunnyBotRunning = true;
    bool projectInitialized = false;

    printf("starting funny bot ... \n");

    if (Globals::g_pPlayer == nullptr || Globals::g_pWorld == nullptr) {
        if (InitProject() == false) {
            std::cout << "Failed to init project" << std::endl;
            projectInitialized = false;
        }
        else {
            projectInitialized = true;
        }
    }
    else {
        projectInitialized = true;
    }

    if (projectInitialized == true) {
        printf("send GetBotStatus \n");

        // tell ui we have initialized project and started bot
        pipeQueue_mutex.lock();
        pipeQueue.push_back(std::string("[GetBotStatus]"));
        pipeQueue_mutex.unlock();

        printf("start .. \n");

        auto g_pPlayer = Globals::g_pPlayer;
        auto world = Globals::g_pWorld;

        g_pPlayer->Update();
        world->Update();

        world->ClearFocus();
        Sleep(100);

        Globals::g_pWndMng = new CWndManager(world->addr.handle);
        auto g_DPlay = new CDPClient(world->addr.handle);
        auto g_pWndWorld = new CWndWorld(Globals::g_pWorld->addr.handle);

        Globals::StartingPoint = g_pPlayer->GetPos();
        printf("FunnyBot [] startPos: %f %f %f\n", Globals::StartingPoint.x, Globals::StartingPoint.y, Globals::StartingPoint.z);

        auto m_objmap = ExternalMap<DWORD, uint32_t>(world->addr.handle, Globals::base + OFFSET_CPROJECT_OBJMAP);
        auto botLogic = new BotLogic(m_objmap);
        bool bForceAttack = false;

        int iBlcokedByIntersectionCount = 0;

        while (true) {
            if (CancelFunnyBot == true) {
                break;
            }

            g_pPlayer->Update();
            auto posToCmp = g_pPlayer->GetPos();

            //printf("main [] IsSelectedTargetStillValid bForceAttack %s\n", bForceAttack ? "true" : "false");
            auto [isStillValid, isBlockedByIntersection] = botLogic->IsSelectedTargetStillValid(bForceAttack);
            if (isStillValid) {
                isBlockedByIntersection = 0; // reset clocking counter check
                Sleep(200);

                // check if someone else is attacking
                auto [validMovers, objmap] = botLogic->GetValidMoversAndMap();
                auto attackingUs = botLogic->GetWhoIsAttackingMe(validMovers);

                bool alreadyAttacking = false;
                auto selectedTarget = CMover(world->GetObjFocus());

                for (auto t : attackingUs) {
                    auto tm = CMover(t);

                    if (t->GetId() == selectedTarget.GetId()) {
                        alreadyAttacking = true;
                        break;
                    }
                }

                if (alreadyAttacking == false && attackingUs.empty() == false) {
                    printf("main [] someone started to attack us \n");
                    bForceAttack = false;
                    world->ClearFocus();
                }
                else {
                    // click again on attack
                    SendAttackKey(false);
                }

                // clear
                for (auto ctrl : objmap) { delete ctrl; }
            }
            else if (isBlockedByIntersection) {
                iBlcokedByIntersectionCount += 1;
                printf("main [] isBlockedByIntersection %d\n", iBlcokedByIntersectionCount);

                if (iBlcokedByIntersectionCount > 2) {
                    world->ClearFocus();
                }
                else {
                    Sleep(200);
                }
            }
            else {
                //printf("main [] GetNextTarget\n");
                Globals::g_pWorld->ClearFocus();
                auto [target, forceAttack] = botLogic->GetNextTarget();
                if (nullptr == target) {
                    if (Globals::ReturnToStartingPoint == false) {
                        printf("main [] ReturnToStartingPoint is false and no target, just waiting now 5s\n");

                        for (int i = 0; i < 50; i++, Sleep(100)) {
                            if (CancelFunnyBot == true) {
                                printf("main [] CancelFunnyBot called\n");
                                break;
                            }
                        }
                    }
                    else {
                        printf("getting back where we started %f %f %f ... \n", Globals::StartingPoint.x, Globals::StartingPoint.y, Globals::StartingPoint.z);
                        g_pPlayer->SetDestPos(Globals::StartingPoint);

                        for (int checkCounter = 0, checkCounter2 = 0; ; ++checkCounter, ++checkCounter2) {
                            if (CancelFunnyBot == true) {
                                printf("main [] CancelFunnyBot called\n");
                                break;
                            }

                            // check if we have reached pos
                            g_pPlayer->Update();
                            auto curPos = g_pPlayer->GetPos();

                            if (std::abs(curPos.x - Globals::StartingPoint.x) < 0.1 && std::abs(curPos.z - Globals::StartingPoint.z) < 0.5f) {
                                break;
                            }

                            // every 3 sec just set dest again
                            if (checkCounter > 30) {
                                checkCounter = 0;
                                g_pPlayer->SetDestPos(Globals::StartingPoint);
                            }

                            // every second check if someone started to attack us
                            if (checkCounter2 > 10) {
                                checkCounter2 = 0;

                                auto [validMovers, objmap] = botLogic->GetValidMoversAndMap();
                                auto attackingUs = botLogic->GetWhoIsAttackingMe(validMovers);
                                if (attackingUs.empty() == false) {
                                    printf("main [] whoa someone is attacking us\n");

                                    // clear
                                    for (auto ctrl : objmap) { delete ctrl; }

                                    break;
                                }
                                else {
                                    // clear
                                    for (auto ctrl : objmap) { delete ctrl; }
                                }
                            }

                            Sleep(100);
                        }
                    }
                }
                else {
                    iBlcokedByIntersectionCount = 0; // reset clocking counter check
                    bForceAttack = forceAttack;
                    auto closestMover = CMover(target);
                    if (closestMover.IsDelete() == FALSE) {
                        printf("main [] %s dist %f m_nHitPoint %d m_nLevel %d bForceAttack %s\n",
                            closestMover.m_szName,
                            GetDist(Globals::g_pPlayer->GetPos(), closestMover.GetPos()),
                            closestMover.m_nHitPoint,
                            closestMover.m_nLevel,
                            bForceAttack ? "true" : "false");

                        //BOOL res = world->CheckSelect((PVOID)target->addr.addr);
                        //printf("main [] CheckSelect %d\n", res);

                        g_pWndWorld->OnLButtonDown(0x000001f4, 0x000001f8);

                        Sleep(10);

                        world->SetObjFocus(target);

                        //printf("main [] SendSetTarget\n");
                        //g_DPlay->SendSetTarget(target->GetId(), 2);
                        //g_DPlay->SendMoverFocus(target->GetId());

                        Sleep(40);

                        SendAttackKey();

                        Sleep(100);
                    }
                    else {
                        Sleep(100);
                    }
                }

                if (target != nullptr) {
                    delete target;
                }
            }
        }
    }
    else {
        printf("project didn't get initialized \n");
    }

    FunnyBotRunning = false;

    pipeQueue_mutex.lock();
    pipeQueue.push_back(std::string("[GetBotStatus]"));
    pipeQueue_mutex.unlock();
}

void TestProject() {
    auto g_pPlayer = Globals::g_pPlayer;
    auto world = Globals::g_pWorld;

    printf("focus %08X\n", world->GetObjFocus()->addr.value);
    printf("\n");


    auto focusObj = new CMover(world->GetObjFocus());
    if (focusObj->addr.value != 0) {
        //g_pPlayer->CMD_SetMeleeAttack(focusObj->GetId());

        //while (focusObj->m_nHitPoint > 0) {
        //    g_pPlayer->m_idDest << focusObj->GetId();
        //    focusObj->Update();
        //}
    }

    auto m_objmap = ExternalMap<DWORD, uint32_t>(world->addr.handle, Globals::base + OFFSET_CPROJECT_OBJMAP);
    for (auto it = m_objmap.begin(); it != m_objmap.end(); it = m_objmap.next(it)) {
        CCtrl* ctrl = new CCtrl(m_objmap.second(it));
        CMover * mover = new CMover(ctrl);

        if (mover->addr == world->LocalPlayer->addr) {
            printf("=====================\n");
        }
        printf("%d %08X\n", m_objmap.first(it).value, m_objmap.second(it).value);
        printf("m_vPos: %f %f %f\n", ctrl->m_vPos.x, ctrl->m_vPos.y, ctrl->m_vPos.z);
        printf("m_objId: %d %08X\n", ctrl->m_objId, ctrl->m_objId);
        printf("m_nHitPoint: %d\n", mover->m_nHitPoint);
        printf("m_nManaPoint: %d\n", mover->m_nManaPoint);
        printf("m_nLevel: %d\n", mover->m_nLevel);
        printf("m_bPlayer: %s\n", mover->m_bPlayer ? "true  <-----------" : "false");
        printf("m_dwType: %d\n", mover->m_dwType);
        printf("IsDelete: %d\n", mover->IsDelete());
        printf("m_dwStateFlag: %d\n", mover->m_pActMover->m_dwStateFlag);
        //printf("m_fArrivalRange: %f\n", mover->m_fArrivalRange);
        printf("m_szName: %s\n", mover->m_szName);
        printf("m_idLastHitMover: %d %s\n", mover->m_idLastHitMover, mover->m_idLastHitMover != -1 ? "      <<<" : "");

        D3DXVECTOR3 vStart = world->LocalPlayer->GetPos();      vStart.y += 0.5f;
        D3DXVECTOR3 vEnd = mover->GetPos();                     vEnd.y += 0.5f;

        if (world->IntersectObjLine(NULL, vStart, vEnd, FALSE, FALSE)) {
            printf("[IntersectObjLine]\n");
        }

        printf("\n");

        world->SetObjFocus(ctrl);

        delete mover;
        delete ctrl;

        //break;
    }

    printf("focus %08X\n", world->GetObjFocus()->addr.value);
}


void InitAndText() {
    InitAndTextRunning = true;

    if (InitProject()) {
        TestProject();
    }
    else {
        std::cout << "Failed to init project" << std::endl;
    }

    InitAndTextRunning = false;
}


void GetTargets() {
    GetTargetsRunning = true;
    bool projectInitialized = false;

    if (Globals::g_pPlayer == nullptr || Globals::g_pWorld == nullptr) {
        if (InitProject() == false) {
            std::cout << "Failed to init project" << std::endl;
            projectInitialized = false;
        }
        else {
            projectInitialized = true;
        }
    }
    else {
        projectInitialized = true;
    }

    if (projectInitialized == true) {

        std::string writeToPipe;
        writeToPipe.append("[TargetsInView]\n");

        auto g_pPlayer = Globals::g_pPlayer;
        auto world = Globals::g_pWorld;

        auto m_objmap = ExternalMap<DWORD, uint32_t>(world->addr.handle, Globals::base + OFFSET_CPROJECT_OBJMAP);
        for (auto it = m_objmap.begin(); it != m_objmap.end(); it = m_objmap.next(it)) {
            CCtrl* ctrl = new CCtrl(m_objmap.second(it));
            CMover* mover = new CMover(ctrl);

            char buff[1024];

            //sprintf(buff, "%d %08X\n", m_objmap.first(it).value, m_objmap.second(it).value); writeToPipe.append(buff);
            //sprintf(buff, "m_vPos: %f %f %f\n", ctrl->m_vPos.x, ctrl->m_vPos.y, ctrl->m_vPos.z); writeToPipe.append(buff);
            //sprintf(buff, "m_objId: %d %08X\n", ctrl->m_objId, ctrl->m_objId); writeToPipe.append(buff);
            //sprintf(buff, "m_nHitPoint: %d\n", mover->m_nHitPoint); writeToPipe.append(buff);
            //sprintf(buff, "m_nManaPoint: %d\n", mover->m_nManaPoint); writeToPipe.append(buff);
            //sprintf(buff, "m_nLevel: %d\n", mover->m_nLevel); writeToPipe.append(buff);
            sprintf(buff, "m_bPlayer: %s\n", mover->m_bPlayer ? "true" : "false"); writeToPipe.append(buff);
            sprintf(buff, "m_dwType: %d\n", mover->m_dwType); writeToPipe.append(buff);
            sprintf(buff, "m_szName: %s\n", mover->m_szName); writeToPipe.append(buff);
            sprintf(buff, "m_idLastHitMover: %d\n", mover->m_idLastHitMover); writeToPipe.append(buff);

            if (mover->addr == world->LocalPlayer->addr) {
                sprintf(buff, "[LocalPlayer]\n"); writeToPipe.append(buff);
            }

            D3DXVECTOR3 vStart = world->LocalPlayer->GetPos();      vStart.y += 0.5f;
            D3DXVECTOR3 vEnd = mover->GetPos();                     vEnd.y += 0.5f;

            if (world->IntersectObjLine(NULL, vStart, vEnd, FALSE, FALSE)) {
                sprintf(buff, "[IntersectObjLine]\n"); writeToPipe.append(buff);
            }

            sprintf(buff, "\n"); writeToPipe.append(buff);

            delete mover;
            delete ctrl;
        }

        std::lock_guard<std::mutex> guard(writePipeQueue_mutex);
        writePipeQueue.push_back(writeToPipe);
    }
    else {
        printf("project didn't get initialized \n");
    }

    GetTargetsRunning = false;
}

bool IsPipeClosed(HANDLE pipeHandle)
{
    DWORD bytesRead;
    BYTE buffer[1];

    BOOL success = PeekNamedPipe(pipeHandle, buffer, sizeof(buffer), nullptr, nullptr, nullptr);
    return (success == 0 && GetLastError() == ERROR_BROKEN_PIPE);
}

void SessionThread() {

    HANDLE pipe = CreateNamedPipeA(
        "\\\\.\\pipe\\CWstdmaptestv0.1Out",            // Pipe name
        PIPE_ACCESS_OUTBOUND,                // Pipe open mode
        PIPE_TYPE_BYTE | PIPE_READMODE_BYTE, // Pipe mode
        1,                                   // Number of instances
        0,                                   // Out buffer size
        0,                                   // In buffer size
        0,                                   // Timeout value
        NULL                                 // Security attributes
    );

    if (pipe == INVALID_HANDLE_VALUE)
    {
        std::cout << "Failed to create named pipe out." << std::endl;
        return;
    }

    std::cout << "Waiting for connection..." << std::endl;

    if (ConnectNamedPipe(pipe, NULL) != FALSE)
    {
        std::cout << "Connected!\n";
    }

    for (;; std::this_thread::sleep_for(std::chrono::milliseconds(10))) {

        // check if pipe is closed
        //if (IsPipeClosed(pipe)) {
        //    printf("pipe was closed\n");
        //    exit(0);
        //}

        std::lock_guard<std::mutex> guard(pipeQueue_mutex);

        if (pipeQueue.empty() == false) {
            auto sBuffer = pipeQueue.front();
            bool removeQueue = true;

            if (sBuffer == "[TestProject]") {
                if (InitAndTextRunning == false) {
                    if (InitAndTextThread.joinable()) { InitAndTextThread.join(); }
                    InitAndTextThread = std::thread(InitAndText);
                }
                else {
                    removeQueue = false;
                }
            }
            else if (sBuffer == "[GetTargets]") {
                if (GetTargetsRunning == false) {
                    if (GetTargetsThread.joinable()) { GetTargetsThread.join(); }
                    GetTargetsThread = std::thread(GetTargets);
                }
                else {
                    removeQueue = false;
                }
            }
            else if (sBuffer.starts_with("[AddTargetToKill]")) {
                auto targetName = sBuffer.substr(std::string("[AddTargetToKill]\n").length());
                bool addTarget = true;

                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
                for (auto target : Globals::TargetsToKill) {
                    if (target == targetName) {
                        std::cerr << "target " << targetName << " already exists" << std::endl;
                        addTarget = false;
                        break;
                    }
                }

                if (addTarget) {
                    Globals::TargetsToKill.push_back(targetName);
                }

                ReadWriteConfig::g_rwConfig.UpdateToConfig();
            }
            else if (sBuffer.starts_with("[RemoveTargetToKill]")) {
                auto targetName = sBuffer.substr(std::string("[RemoveTargetToKill]\n").length());
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
                Globals::TargetsToKill.erase(std::remove_if(Globals::TargetsToKill.begin(), Globals::TargetsToKill.end(),
                    [&targetName](const std::string& target) { return target == targetName; }),
                    Globals::TargetsToKill.end());

                ReadWriteConfig::g_rwConfig.UpdateToConfig();
            }
            else if (sBuffer.starts_with("[KeyToAttack]")) {
                auto keyToAttack = std::stoi(sBuffer.substr(std::string("[KeyToAttack]\n").length()));
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
                Globals::AttackKey = keyToAttack;

                ReadWriteConfig::g_rwConfig.UpdateToConfig();
            }
            else if (sBuffer == "[GetKeyToAttack]") {
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);

                std::string writeToPipe;
                writeToPipe.append("[KeyToAttack]\n");
                writeToPipe.append(std::to_string(Globals::AttackKey));

                std::lock_guard<std::mutex> guard(writePipeQueue_mutex);
                writePipeQueue.push_back(writeToPipe);
            }
            else if (sBuffer == "[GetTargetsToKill]") {
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
                std::string writeToPipe;
                writeToPipe.append("[TargetsToKill]\n");

                for (auto target : Globals::TargetsToKill) {
                    writeToPipe.append(target);
                    writeToPipe.append("\n");
                }

                std::lock_guard<std::mutex> guard(writePipeQueue_mutex);
                writePipeQueue.push_back(writeToPipe);
            }
            else if (sBuffer == "[GetBotStatus]") {
                std::string writeToPipe;
                writeToPipe.append("[BotStatus]\n");
                writeToPipe.append(FunnyBotRunning ? "true" : "false");

                std::lock_guard<std::mutex> guard(writePipeQueue_mutex);
                writePipeQueue.push_back(writeToPipe);
            }
            else if (sBuffer == "[EnableReturnToStartingPoint]") {
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
                Globals::ReturnToStartingPoint = true;
                ReadWriteConfig::g_rwConfig.UpdateToConfig();
            }
            else if (sBuffer == "[DisableReturnToStartingPoint]") {
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);
                Globals::ReturnToStartingPoint = false;
                ReadWriteConfig::g_rwConfig.UpdateToConfig();
            }
            else if (sBuffer == "[GetReturnToStartingPoint]") {
                std::lock_guard<std::mutex> guardTargetsToKill(Globals::Config_mutex);

                std::string writeToPipe;
                writeToPipe.append("[ReturnToStartingPoint]\n");
                writeToPipe.append(Globals::ReturnToStartingPoint ? "true" : "false");

                std::lock_guard<std::mutex> guard(writePipeQueue_mutex);
                writePipeQueue.push_back(writeToPipe);
            }
            else if (sBuffer == "[StartBot]") {
                if (FunnyBotRunning == false) {
                    if (FunnyBotThread.joinable()) { FunnyBotThread.join(); }

                    printf("waiting threads ... \n");

                    if (InitAndTextThread.joinable()) { InitAndTextThread.join(); }
                    if (GetTargetsThread.joinable()) { GetTargetsThread.join(); }

                    FunnyBotThread = std::thread(FunnyBot);
                }
                else {
                    removeQueue = false;
                }
            }
            else if (sBuffer == "[StopBot]") {
                CancelFunnyBot = true;
            }
            else if (sBuffer == "[Close]") {
                printf("exit\n");
                exit(0);
            }

            if (removeQueue) {
                pipeQueue.erase(pipeQueue.begin());
            }

        }

        // check what to write
        std::lock_guard<std::mutex> guard2(writePipeQueue_mutex);
        if (writePipeQueue.empty() == false) {
            for (auto msg : writePipeQueue) {
                DWORD bytesWritten;
                if (WriteFile(pipe, msg.c_str(), msg.length() + 1, &bytesWritten, NULL)) {
                    //std::cout << "Sent data to C#.\n";
                }

                FlushFileBuffers(pipe);
            }
            writePipeQueue.clear();
        }

    }
}

int main(int argc, char* argv[]) {
    printf("start\n");
    
    ReadWriteConfig::g_rwConfig.UpdateFromConfig();
    /* // for testing without ui
    if (InitProject()) {
        //TestProject();
        //auto m_objmap = ExternalMap<DWORD, uint32_t>(Globals::g_pWorld->addr.handle, Globals::base + OFFSET_CPROJECT_OBJMAP);
        //Globals::g_pWorld->SetObjFocus();
        
        //printf("m_pObjFocusCheck.addr.value %08X\n", Globals::g_pWorld->m_pObjFocusCheck.addr.value);
        //printf("m_pObjFocusCheck.addr + 0x2e8 %08X \n", (Globals::g_pWorld->m_pObjFocusCheck.addr + 0x2e8).value);
        //printf("focus %08X\n", Globals::g_pWorld->GetObjFocus()->GetId());
        //printf("0x170 %08X\n", (Globals::g_pWorld->GetObjFocus()->addr + 0x170).value);
        //printf("0x174 %08X\n", (Globals::g_pWorld->GetObjFocus()->addr + 0x174).value);

        //auto asd2 = ExternalVariable<uint32_t>(Globals::g_pWorld->handle, Globals::base + (0x00fe8748 - 0xd0000));
        //printf("asd2 %08X\n", (asd2).value);
        //printf("time %d\n", CTime::GetCurrentTime());

        //time_t t = CTime::GetCurrentTime().GetTime();
        //CTimeSpan ts(t);

        //printf("time %d\n", ts.GetSeconds());

        printf("player %d\n", Globals::g_pPlayer->addr);
        printf("player id %d\n", Globals::g_pPlayer->GetId());

        //printf("asd2 166c %08X\n", (asd2 + 0x166c).value);

        //printf("key: ");
        //auto rjandelkey = ExternalVariable<uint8_t[16]>(Globals::g_pWorld->handle, Globals::base + (0x00ff83e8 - 0xd0000));
        //for (auto k : rjandelkey.value) {
        //    printf("\\x%02X", k);
        //}
        //printf("\n");

        auto g_DPlay = new CDPClient(Globals::g_pWorld->addr.handle);
        auto g_pWndWorld = new CWndWorld(Globals::g_pWorld->addr.handle);

        //auto buffCommand = std::string("/BuffPlayer ") + (char*)Globals::g_pPlayer->m_szBuffId;
        //g_pWndWorld->OnLButtonDown(0x000001f4, 0x000001f8);
        //Sleep(10);
        //auto data = ExternalVariable<uint8_t[0x2000]>(Globals::g_pWorld->handle, 0x383fedc0).value;
        //g_DPlay->SendChatMsg();

        //for (;; Sleep(100)) {
        //}

        
        
        Globals::g_pPlayer->Update();
        Globals::StartingPoint = Globals::g_pPlayer->GetPos();
        auto m_objmap = ExternalMap<DWORD, uint32_t>(Globals::g_pWorld->addr.handle, Globals::base + OFFSET_CPROJECT_OBJMAP);
        auto botLogic = new BotLogic(m_objmap);
        auto [target, forceAttack] = botLogic->GetNextTarget();
        if (target != nullptr) {
            //auto dif = ExternalVariable<uint32_t>(Globals::g_pWorld->addr.handle, Globals::base + OFFSET_CLOCK).value - Globals::g_pWorld->m_pClockCheck.value;
            //Globals::g_pWorld->m_pClockCheck << ExternalVariable<uint32_t>(Globals::g_pWorld->addr.handle, Globals::base + OFFSET_CLOCK);
            //Globals::g_pWorld->m_pClockCheck2 << Globals::g_pWorld->m_pClockCheck2.value + dif;

            //g_DPlay->SendMouseLeftClickOnCWndWorld();

            g_pWndWorld->OnLButtonDown(0x000001f4, 0x000001f8);

            Sleep(10);
            Globals::g_pWorld->SetObjFocus(target);
            Sleep(400);
        }

        
        auto wndMng = new CWndManager(Globals::g_pWorld->addr.handle);

        auto m_pWndTaskBar = ExternalVariable<uint32_t>(Globals::g_pWorld->addr.handle, Globals::base + OFFSET_WND_TASKBAR);
        auto m_paSlotItem = m_pWndTaskBar + (OFFSET_WND_TASKBAR_SLOTITEM + (2 * SLOTITEM_SIZE));

        wndMng->ObjectExecutor(m_paSlotItem.addr);
        

        //printf("name : %s\n", Globals::g_pPlayer->m_szName);
        return 0;
    }
    */

    system("start UI.exe");

    //pipeQueue.push_back(std::string("[StartBot]"));
    std::thread sessionThread(SessionThread);

    HANDLE pipe = CreateNamedPipeA(
        "\\\\.\\pipe\\CWstdmaptestv0.1In",             // Pipe name
        PIPE_ACCESS_INBOUND,                  // Pipe open mode
        PIPE_TYPE_BYTE | PIPE_READMODE_BYTE, // Pipe mode
        1,                                   // Number of instances
        0,                                   // Out buffer size
        0,                                   // In buffer size
        0,                                   // Timeout value
        NULL                                 // Security attributes
    );

    if (pipe == INVALID_HANDLE_VALUE) {
        std::cerr << "Failed to create named pipe in.";
        return 1;
    }

    std::cout << "Waiting for connection...\n";

    if (ConnectNamedPipe(pipe, NULL) != FALSE)
    {
        std::cout << "Connected!\n";

        for (;; Sleep(2)) {
            char buffer[10240];
            DWORD bytesRead;
            if (ReadFile(pipe, buffer, sizeof(buffer), &bytesRead, NULL))
            {
                std::cout << "Received data: " << buffer << std::endl;
            }

            auto sBuffer = std::string(buffer);

            if (sBuffer.empty() == true) {
                std::cout << "pipe end\n";
                break;
            }

            std::lock_guard<std::mutex> guard(pipeQueue_mutex);
            pipeQueue.push_back(sBuffer);
        }
        
        DisconnectNamedPipe(pipe);
    }

    CloseHandle(pipe);
    

    // system("pause");

    return 0;
}

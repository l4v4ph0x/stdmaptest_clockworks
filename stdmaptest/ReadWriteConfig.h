#pragma once

#include <stdint.h>
#include <vector>
#include <string>
#include <functional>

#include <iostream>
#include <istream>
#include <fstream>
#include "msgpack/include/msgpack.hpp"

#include "Config.h"
#include "Globals.h"

#define PB_CONFIG_FILE "config.bin"
#define PB_STRING_TO_C(s) (s.c_str() - 8)


class ReadWriteConfig {
private:
    CConfig deserializedData;

public:

    ReadWriteConfig() {
        
    }

    bool Update() {
        bool res = false;

        // Read the serialized data from the file
        std::ifstream readFile(PB_CONFIG_FILE, std::ios::binary);
        if (readFile) {
            // Determine the size of the file
            readFile.seekg(0, std::ios::end);
            std::streamsize fileSize = readFile.tellg();
            readFile.seekg(0, std::ios::beg);

            // Read the data into a buffer
            std::vector<uint8_t> readData(fileSize);
            if (readFile.read(reinterpret_cast<char*>(readData.data()), fileSize)) {
                // Deserialize the data
                //RootObjects deserializedData;
                deserializedData = CConfig{};
                msgpack::object_handle oh =
                    msgpack::unpack(reinterpret_cast<const char*>(readData.data()), fileSize);
                msgpack::object obj = oh.get();
                obj.convert(deserializedData);
            }
            else {
                std::cerr << "Failed to read data from file." << std::endl;
                readFile.close();
                return false;
            }
        }
        else {
            std::cerr << "Failed to open file for reading." << std::endl;
            return false;
        }


        res = true;

        if (res == true) {
        }

        return res;
    }

    bool WriteUpdatesToFile() {
        // Serialize the data
        std::vector<uint8_t> packedData;
        msgpack::sbuffer buffer;
        msgpack::pack(buffer, deserializedData);
        packedData.assign(buffer.data(), buffer.data() + buffer.size());

        // Write the serialized data to a file
        std::ofstream file(PB_CONFIG_FILE, std::ios::binary);
        if (file) {
            file.write(reinterpret_cast<const char*>(packedData.data()), packedData.size());
            file.close();
        }
        else {
            std::cerr << "Failed to open file for writing." << std::endl;
            return false;
        }

        return true;
    }

    void UpdateToConfig() {
        deserializedData.targetsToKill.clear();
        for (auto d : Globals::TargetsToKill) {
            deserializedData.targetsToKill.push_back(d);
        }

        deserializedData.keytoKill = Globals::AttackKey;
        deserializedData.returnToStartingPoint = Globals::ReturnToStartingPoint;

        WriteUpdatesToFile();
    }

    void UpdateFromConfig() {
        Update();

        Globals::TargetsToKill.clear();
        for (auto d : deserializedData.targetsToKill) {
            Globals::TargetsToKill.push_back(d);
        }

        Globals::AttackKey = deserializedData.keytoKill;
        Globals::ReturnToStartingPoint = deserializedData.returnToStartingPoint;
    }


    static ReadWriteConfig g_rwConfig;

};

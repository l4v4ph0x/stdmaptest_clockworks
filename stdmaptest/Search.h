#pragma once

#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>

class Search {

public:

	static unsigned long GetProc(const wchar_t* name) {
		void* snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		PROCESSENTRY32W pe32;
		pe32.dwSize = sizeof(PROCESSENTRY32W);

		while (Process32NextW(snapshot, &pe32))
			if (_wcsicmp(pe32.szExeFile, name) == 0)
				return pe32.th32ProcessID;

		return 0;
	}

    static unsigned long GetModule(void* handle, const wchar_t* name) {
        HMODULE hMods[1024];
        DWORD cbNeeded;
        unsigned int i;

        if (EnumProcessModules(handle, hMods, sizeof(hMods), &cbNeeded)) {
            for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++) {
                TCHAR szModName[MAX_PATH];

                if (GetModuleFileNameExW(handle, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR))) {
                    unsigned int j;
                    for (j = lstrlenW(szModName) - 1; j > 0; j--) {
                        if (szModName[j] == '\\') {
                            j++;
                            break;
                        }
                    }

                    if (_wcsicmp(szModName + j, name) == 0) {
                        return (unsigned long)hMods[i];
                    }
                }
            }
        }

        return 0;
    }

    static unsigned long GetMainThreadId(unsigned long dwPid) {
        // Get the main thread ID of the process
        DWORD threadId = 0;
        THREADENTRY32 te32;
        te32.dwSize = sizeof(THREADENTRY32);

        HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
        if (hSnapshot != INVALID_HANDLE_VALUE) {
            if (Thread32First(hSnapshot, &te32)) {
                do {
                    if (te32.th32OwnerProcessID == dwPid) {
                        threadId = te32.th32ThreadID;
                        break;
                    }
                } while (Thread32Next(hSnapshot, &te32));
            }
            CloseHandle(hSnapshot);
        }

        return threadId;
    }

    struct handle_data {
        unsigned long process_id;
        HWND window_handle;
    };

    static BOOL is_main_window(HWND handle) {
        return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
    }

    static BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam) {
        handle_data& data = *(handle_data*)lParam;
        unsigned long process_id = 0;
        GetWindowThreadProcessId(handle, &process_id);
        if (data.process_id != process_id || !is_main_window(handle))
            return TRUE;
        data.window_handle = handle;
        return FALSE;
    }

    static HWND find_main_window(unsigned long process_id) {
        handle_data data;
        data.process_id = process_id;
        data.window_handle = 0;
        EnumWindows(enum_windows_callback, (LPARAM)&data);
        return data.window_handle;
    }



};

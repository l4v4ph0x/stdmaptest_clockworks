#pragma once

#include "all.h"
#include <mutex>
#include <vector>

#include "Common/Mover.h"
#include "Common/World.h"

class CWndManager;

class Globals {
public:
	static uint32_t base;
	static std::vector<PVOID> ThreadHandles;
	static CMover* g_pPlayer;
	static CWorld * g_pWorld;
	static CWndManager* g_pWndMng;
	static D3DXVECTOR3 StartingPoint;

	static std::vector<std::string> TargetsToKill;
	static int AttackKey;
	static bool ReturnToStartingPoint;
	static std::mutex Config_mutex;
};

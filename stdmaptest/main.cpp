#include <Windows.h>
#include <stdio.h>

int main(int argc, char* argv[]) {

  std::map<DWORD, PVOID> *m_map = (std::map<DWORD, PVOID>*)(0x00E06664);

  int i = 0;
  for (auto it = m_map->begin(); it != m_map->end(); it = std::next(it)) {
    printf("%d %08X\n", it->first, it->second);
    // m_mapBuffer[i] = (uint32_t)it->second;
    i++;
  }

  return 0;
}

#pragma once

#include <Windows.h>
#include <stdint.h>
#include <type_traits>

#include <stdio.h>


template<typename T>
struct ExternalVariable
{

public:
    PVOID handle;
    uint32_t addr;

    T value;
    bool initialized = false;

    static ExternalVariable<T> Alloc(PVOID handle) {
        PVOID loc = VirtualAllocEx(handle, 0, sizeof(T), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
        return ExternalVariable<T>(handle, (uint32_t)loc);
    }

    void Free() {
        VirtualFreeEx(handle, (PVOID)addr, 0, MEM_RELEASE);
    }


    ExternalVariable()
    {
        initialized = false;
    }

    ExternalVariable(PVOID handle, uint32_t addr) : handle(handle)
                                                  , addr(addr)
    {
        update();
        initialized = true;
    }


    void update() {
        ReadProcessMemory(handle, (PVOID)addr, &value, sizeof(T), 0);
    }


    /*
    template <typename = typename std::enable_if<!std::is_array<T>::value>::type>
    T operator()() {
        update();
        return value;
    }
    */
    
    template<typename U>
    ExternalVariable<U> operator = (ExternalVariable<U> val)
    {
        val.update();
        handle = val.handle;
        addr = val.addr;
        return to<U>();
    }


    ExternalVariable<T> p() {
        update();
        return ExternalVariable<T>(handle, value);
    }

    ExternalVariable<T> operator[](int index) {
        update();
        return ExternalVariable<T>(handle, value + (index * sizeof(T)));
    }

    ExternalVariable<T> operator+(uint32_t val) {
        update();
        return ExternalVariable<T>(handle, value + val);
    }

    ExternalVariable<T> operator-(uint32_t val) {
        update();
        return ExternalVariable<T>(handle, value - val);
    }

    bool operator==(ExternalVariable val) {
        update();
        return value == val.value;
    }

    template<typename U>
    bool operator==(U val) {
        update();
        return value == val;
    }


    bool operator!=(ExternalVariable val) {
        update();
        return value != val.value;
    }

    template<typename U>
    bool operator!=(U val) {
        update();
        return value != val;
    }

    ExternalVariable<T> operator<<(ExternalVariable val) {
        WriteProcessMemory(handle, (PVOID)addr, &val.value, sizeof(val.value), 0);
        update();
        return *this;
    }

    template<typename U>
    ExternalVariable<T> operator<<(U val) {
        WriteProcessMemory(handle, (PVOID)addr, &val, sizeof(U), 0);
        update();
        return *this;
    }

    template<typename U, size_t N>
    ExternalVariable<T> operator<<(U(&arr)[N]) {
        WriteProcessMemory(handle, (PVOID)addr, &arr, (sizeof(U) * N), 0);
        update();
        return *this;
    }



    template<typename U>
    ExternalVariable<U> to() {
        return ExternalVariable<U>(handle, addr);
    }
};


/*
template<typename T>
struct CMemory {
private:
    PVOID handle;
public:
    CMemory(PVOID handle) : handle(handle)
    {
    }

    T operator<<(uint32_t addr);

    CMemory<uint32_t> u32() {
        return CMemory<uint32_t>(handle);
    }

    CMemory<uint16_t> u16() {
        return CMemory<uint16_t>(handle);
    }

    CMemory<uint8_t> u8() {
        return CMemory<uint8_t>(handle);
    }

    template<typename U>
    U r(uint32_t addr) {
        U out = 0;
        ReadProcessMemory(handle, (PVOID)addr, &out, sizeof(U), 0);
        return out;
    }

    template<typename U>
    ExternalVariable<U> v(uint32_t addr) {
        return ExternalVariable<U>(handle, addr);
    }
};

template<typename T>
T CMemory<T>::operator<<(uint32_t addr) {
    T out = 0;
    ReadProcessMemory(handle, (PVOID)addr, &out, sizeof(T), 0);
    return out;
}
*/

#pragma once

#include "../all.h"

DWORD RunFunctionExternally(PVOID handle, PVOID localFunction, LPVOID lpParameter, SIZE_T parameterSize, bool simulate = false, bool suspendMainThread = false);

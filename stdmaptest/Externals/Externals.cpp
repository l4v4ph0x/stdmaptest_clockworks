#pragma once

#include "../all.h"
#include "Externals.h"
#include "../Globals.h"

#include <iostream>
#include <vector>
#include <map>


void ExternalHook(PVOID handle, PVOID detourFunction, uint32_t addrFrom) {

    auto hookLen = getFunlength(detourFunction);
    void* loc = VirtualAllocEx(handle, 0, hookLen, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

    //printf("hook [] hookPtr %08X hookLen %08X loc %08X\n", detourFunction, hookLen, loc);

    WriteProcessMemory(handle, loc, detourFunction, hookLen, 0);

    DWORD curProtection;
    VirtualProtectEx(handle, (PVOID)loc, 5, PAGE_EXECUTE_READWRITE, &curProtection);

    uint8_t opcodes[5]{ 0xE9, 0x00, 0x00, 0x00, 0x00 };
    int rel = ((uint32_t)loc - addrFrom) - 5;
    *(int*)(opcodes + 1) = rel;

    ExternalVariable<uint8_t[5]>(handle, addrFrom) << opcodes;

    DWORD temp;
    VirtualProtectEx(handle, (PVOID)loc, 5, curProtection, &temp);
}



struct ExternalFunctionInfo {
    PVOID localFunction;
    int funLen;
    PVOID funLoc;
    PVOID argsLoc;
};

std::vector<ExternalFunctionInfo *> CachedExternalFunctions;


bool TestMemory(PVOID handle, PVOID addr) {
    MEMORY_BASIC_INFORMATION memInfo;
    SIZE_T result = VirtualQueryEx(handle, addr, &memInfo, sizeof(memInfo));
    if (result == 0) {
        // Failed to query the memory information
        // Handle the error
        return false;
    }

    if (memInfo.State == MEM_COMMIT || (memInfo.State == MEM_COMMIT | MEM_RESERVE)) {
        DWORD protection = memInfo.Protect;

        // Check if the protection includes PAGE_EXECUTE_READWRITE
        if ((protection & PAGE_EXECUTE_READWRITE) == PAGE_EXECUTE_READWRITE) {
            return true;
        }
    }
    else if (memInfo.State == MEM_RESERVE) {
        // The address is reserved but not committed
    }
    else if (memInfo.State == MEM_FREE) {
        // The address is not valid
    }
    else {
        // Other possible states
    }

    return false;
}

DWORD RunFunctionExternally(PVOID handle, PVOID localFunction, LPVOID lpParameter, SIZE_T parameterSize, bool simulate, bool suspendMainThread) {
    //printf("RunFunctionExternally [] Begin\n");

    ExternalFunctionInfo * foundItem = nullptr;
    for (auto it : CachedExternalFunctions) {
        if (it->localFunction == localFunction) {
            foundItem = it;
            break;
        }
    }

    int funLen;
    PVOID funLoc;
    PVOID argsLoc;

    if (foundItem == nullptr) {
        funLen = getFunlength(localFunction);
        funLoc = VirtualAllocEx(handle, 0, funLen, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
        argsLoc = VirtualAllocEx(handle, 0, funLen, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

        if (WriteProcessMemory(handle, funLoc, localFunction, funLen, 0) == FALSE) {
            std::cerr << "RunFunctionExternally [] WriteProcessMemory 1: " << GetLastError() << std::endl;
            return 0;
        }

        auto it = new ExternalFunctionInfo{ localFunction, funLen, funLoc, argsLoc };
        CachedExternalFunctions.push_back(it);
    }
    else {
        funLen = foundItem->funLen;
        funLoc = foundItem->funLoc;
        argsLoc = foundItem->argsLoc;
    }

    if (funLoc == nullptr || argsLoc == nullptr || localFunction == nullptr) { 
        std::cerr << "RunFunctionExternally [] nullptr: " << GetLastError() << std::endl;
        // TODO: also free its addresses
        CachedExternalFunctions.clear();
        return 0; 
    }
    
    if (WriteProcessMemory(handle, argsLoc, lpParameter, parameterSize, 0) == FALSE) {
        std::cerr << "RunFunctionExternally [] WriteProcessMemory 2: " << GetLastError() << std::endl;
        // TODO: also free its addresses
        CachedExternalFunctions.clear();
        return 0; 
    }

    if (simulate) {
        printf("RunFunctionExternally [] argsLoc %08X funLen %08X funLoc %08X\n", argsLoc, funLen, funLoc);
        return 0;
    }

    if (TestMemory(handle, argsLoc) == false || TestMemory(handle, funLoc) == false) {
        std::cerr << "RunFunctionExternally [] TestMemory failed" << std::endl;
        // TODO: also free its addresses
        CachedExternalFunctions.clear();
        return 0;
    }

    if (suspendMainThread == true) {
        for (auto th : Globals::ThreadHandles) { SuspendThread(th); }
    }

    //printf("RunFunctionExternally [] CreateRemoteThread\n");
    HANDLE hRemoteThread = CreateRemoteThread(handle, 0, 0, (LPTHREAD_START_ROUTINE)funLoc, argsLoc, 0, 0);
    if (hRemoteThread == NULL) {

        if (suspendMainThread == true) {
            for (auto th : Globals::ThreadHandles) { ResumeThread(th); }
        }

        std::cerr << "Failed to create remote thread. Error code: " << GetLastError() << std::endl;
        CachedExternalFunctions.clear();
    }
    else {
        //printf("RunFunctionExternally [] WaitForSingleObject\n");
        DWORD dwWaitResult = WaitForSingleObject(hRemoteThread, 1000);

        if (suspendMainThread == true) {
            for (auto th : Globals::ThreadHandles) { ResumeThread(th); }
        }

        // new version will save to cache to prevent over allocation
        //VirtualFreeEx(handle, funLoc, 0, MEM_RELEASE);
        //VirtualFreeEx(handle, argsLoc, 0, MEM_RELEASE);

        if (dwWaitResult == WAIT_FAILED)
        {
            // handle error
            std::cerr << "RunFunctionExternally [] wait failed" << std::endl;
        }

        // Get exit code of thread (i.e. function return value)
        DWORD dwResult = 0;
        //printf("RunFunctionExternally [] GetExitCodeThread\n");
        if (!GetExitCodeThread(hRemoteThread, &dwResult))
        {
            // handle error
        }

        //printf("RunFunctionExternally [] CloseHandle\n");
        CloseHandle(hRemoteThread);

        return dwResult;
    }

    return -1;
}

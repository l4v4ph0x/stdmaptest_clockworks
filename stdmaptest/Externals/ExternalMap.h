#pragma once

#include "../all.h"

template<typename T1, typename T2>
struct ExternalMap {
private:
    PVOID handle;
    uint32_t addr;
    ExternalVariable<uint32_t> _DATA;

public:
    ExternalMap(PVOID handle, uint32_t addr) : handle(handle)
        , addr(addr)
        , _DATA(ExternalVariable<uint32_t>(handle, addr))
    {
    }

    ExternalVariable<T1> first(ExternalVariable<uint32_t> it) {
        return it[4].to<T1>();
    }

    ExternalVariable<T2> second(ExternalVariable<uint32_t> it) {
        return it[5].to<T2>();
    }


    ExternalVariable<uint32_t> begin() {
        return _DATA[0];
    }

    ExternalVariable<uint32_t> end() {
        return _DATA;
    }

    ExternalVariable<uint32_t> next(ExternalVariable<uint32_t> from) {
        auto ppiVar3 = from;
        auto ppiVar2 = ppiVar3[2];
        auto ppiVar4 = ppiVar3;

        if ((ppiVar2 + 0xd).to<uint8_t>() == '\0') {
            auto cVar1 = (ppiVar2[0] + 0xd).to<uint8_t>();
            ppiVar3 = ppiVar2;
            ppiVar2 = ppiVar2[0];

            while (cVar1 == '\0') {
                cVar1 = (ppiVar2[0] + 0xd).to<uint8_t>();
                ppiVar3 = ppiVar2;
                ppiVar2 = ppiVar2[0];
            }
        }
        else {
            uint8_t bVar5 = 0;
            do {
                ppiVar3 = ppiVar4[1];
                if ((ppiVar3 + 0xd).to<uint8_t>() != '\0') break;

                bVar5 = ppiVar4 == ppiVar3[2];
                ppiVar4 = ppiVar3;
            } while (bVar5);
        }

        return ppiVar3;
    }
};

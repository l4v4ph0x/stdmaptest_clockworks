# Build instructions
you need to install boost (static) your self and include to "AdditionalIncludeDirectories" <br/>
be sure to build with x86 Release, maybe debug works also, i don't remember anymore <br/>
build stdmaptest and UI projects <br/>
copy dotnet filed from ./UI/bin/Release/net6.0-windows/\* to Release where stdmaptest built exe <br/>
<br/>

### Installing boost via vcpkg
install vcpkg: https://github.com/microsoft/vcpkg <br/>
```
git clone https://github.com/microsoft/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
```
install boost: <br/>
```
vcpkg install boost --static
```
<br/>


## Folder structure
Release folder structure should be: <br/>
- stdmaptest.exe
- UI.deps.json
- UI.dll
- UI.exe
<br/>
optinally: <br/>
- config.bin
<br/>
then you can run stdmaptest.exe (that should open pipe and run ui) <br/>
<br/>

## Ghidra definitions
* use 7z to extract parted file GameBinAndGhidra/cw_Neuz_dump.exe.7z.001
Ghidra file (from GameBinAndGhidra) on namespace has defined functions (under C folder):
- CAction
- CActionMover
- CAr (used for client server communication)
- CArchiveStream
- CDPClient
- CDPMng
- CFileException
- CInnerUnknown
- CMapStringToString
- CMenuImages
- CMover
- CNoTrackObject
- CObjMap
- CProject
- CRuntimeClass
- CSettingsStoreSP
- CWndBase
- CWndFiendCtrlEx
- CWndMgr
- CWndNavigator
- CWndWorld
- CWorld